#!/usr/bin/env python
""" 
==============================================================================
title           : db_connector.py
description     : This is the database connector file for vtiger database
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python db_connector.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import MySQLdb
from db_config import *

def get_cursor():
        ''' Get all Product Families from the CRM database '''
        try:
                crm_dbh = MySQLdb.connect(host=crm_host, user=crm_user, passwd=crm_passwd, db=crm_db)
                crm_cursor = crm_dbh.cursor()
		return crm_cursor, crm_dbh
        except MySQLdb.Error, e:
                print "MySQL Error %d: %s" % (e.args[0], e.args[1])
                return False

def get_db_query(sql_query, query_type):
    try:
        crm_cursor, crm_dbh = get_cursor()
        crm_cursor.execute(sql_query)

        if(query_type == 'select'):
            ret_data = crm_cursor.fetchall()
        elif (query_type == 'column'):
            columns = crm_cursor.description
            result = []
            for value in crm_cursor.fetchall():
                tmp = {}
                for (index,column) in enumerate(value):
                    tmp[columns[index][0]] = column
                result.append(tmp)
            return result
        else:
            ret_data = True

        crm_dbh.commit()
        return ret_data
    except MySQLdb.Error, e:
        print "MySQL Error %d: %s" % (e.args[0], e.args[1])
        return False	

 
