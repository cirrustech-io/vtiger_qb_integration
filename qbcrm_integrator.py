#!/usr/bin/env python
""" 
==============================================================================
title           : qbcrm_integrator.py
description     : This file is to fetch data from quickbooks and pushed to vtiger crm
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python qbcrm_integrator.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

from vtiger_connect import get_create, get_retrieve, get_update
import MySQLdb
from quick_books import quick_book_connector, get_truncated_name, get_product_new_xml, generate_qbxml_query
from datetime import datetime, timedelta
from db_config import *
from qbcrm_config import assigned_user_id, crm_module_mapping, currency_id

try:
    import ElementTree as ET
except ImportError:
  try:
    # Python 2.5 need to import a different module
    import xml.etree.ElementTree as ET
  except ImportError:
    exit_err("Failed to import ElementTree from any known place")

def get_crm_data(sql_query):
        ''' Get all Product Families from the CRM database '''
        try:
                crm_dbh = MySQLdb.connect( host=crm_host, user=crm_user, passwd=crm_passwd, db=crm_db )
                crm_cursor = crm_dbh.cursor()

                # Get data from CRM
                crm_cursor.execute(sql_query)
                data = crm_cursor.fetchall()
                return data

        except MySQLdb.Error, e:
                print "MySQL Error %d: %s" % (e.args[0], e.args[1])
                return False

def get_all_product_listid():
    return get_crm_data("select productid, cf_847 from vtiger_productcf;")

def get_missing_product():
    return get_crm_data("select p.productid, p.productname, p.productcode, p.unit_price, p.productcategory from vtiger_products p, vtiger_productcf pcf where p.productid = pcf.productid and pcf.cf_847='' and productcategory!= 'Internal Cost';")

def get_missing_account():
    return get_crm_data("select acc.accountname, acc.accountid from vtiger_account acc, vtiger_accountscf accscf where acc.accountid = accscf.accountid and accscf.cf_809 = '';")

def get_missing_salesorder():
    return get_crm_data("select salesorderid from vtiger_salesordercf where cf_817='';")

def get_missing_potential():
    return get_crm_data("select pot.potentialname, pot.potential_no, pcf.potentialid, acc.accountname from vtiger_potential pot, vtiger_potentialscf pcf, vtiger_account acc where pot.potentialid = pcf.potentialid and pot.related_to = acc.accountid and pcf.cf_894 = '';")

def get_relative_customer(qb_id):
    return get_crm_data("select acc.accountid from vtiger_crmentity crm, vtiger_accountscf acc where acc.cf_809 = '%s' and crm.crmid = acc.accountid and crm.deleted = 0;" %(qb_id))
            
def get_relative_contact(qb_id):
    return get_crm_data("select acc.accountid, con.contactid from vtiger_crmentity crm, vtiger_accountscf acc, vtiger_contactdetails con where acc.cf_809 = '%s' and crm.crmid = acc.accountid and con.accountid=acc.accountid and crm.deleted = 0;" %(qb_id))

def get_relative_potential(qb_id):
    return get_crm_data("select pot.related_to, pot.contact_id, pot.potentialid, accb.bill_street from vtiger_crmentity crm, vtiger_potential pot, vtiger_potentialscf pcf, vtiger_contactdetails con, vtiger_accountbillads accb where pcf.cf_894 = '%s' and crm.crmid = pot.potentialid and pot.contact_id = con.contactid and pot.potentialid = pcf.potentialid and accb.accountaddressid = pot.related_to and crm.deleted = 0;" %(qb_id))

def get_relative_product(qb_id):
    return get_crm_data("select prod.productid, prod.unit_price, prod.qtyinstock, prod.commissionrate, prod.currency_id from vtiger_products prod, vtiger_productcf pcf where prod.productid = pcf.productid and pcf.cf_847 = '%s';" %(qb_id))

def get_data_from_xml(resp_xml, entity_name, crm_id=None):
    # Parse a xml file (specify the path)
   
    try:
        root = ET.fromstring(resp_xml)
        for firstchild in root:
            for secondchild in firstchild:
                for thirdchild in secondchild:
                    tag_dct = {}
                    ret_cust = None
                    for fourthchild in thirdchild:
                        tag_dct[fourthchild.tag] =  fourthchild.text
                        inside_tag_dct = {}
                        if fourthchild.getchildren():
                            for fifthchild in fourthchild:                      
                                inside_tag_dct[fifthchild.tag] =  fifthchild.text
                                if(fourthchild.tag == 'AdditionalContactRef'):
                                    tag_dct[inside_tag_dct['ContactName']] = inside_tag_dct.get('ContactValue','')
                                
                                inside_sixth_dct = {}
                                if fifthchild.getchildren():
                                    for sixthchild in fifthchild:                      
                                        inside_sixth_dct[sixthchild.tag] =  sixthchild.text
                                    inside_tag_dct[fifthchild.tag] = inside_sixth_dct
                                        
                            tag_dct[fourthchild.tag] = inside_tag_dct

                    if(entity_name == 'Customer'):
                        if (tag_dct['Sublevel'] == '0'):
                            if crm_id:
                                cust_dct = update_customer_in_crm(tag_dct, crm_id)
                                print "CUSTOMER UPDATED : ", cust_dct
                            else:
                                cust_dct = create_customer_in_crm(tag_dct)
                                print "CUSTOMER ADDED : ", cust_dct
                            get_contact_from_qb(cust_dct['cf_809'], cust_dct['id'])
                        else:
                            if(tag_dct.has_key('ParentRef')):
                                if crm_id:
                                    pot_dct = update_potential_in_crm(tag_dct, crm_id)
                                    print "POTENTIAL UPDATED : ", pot_dct
##                                else:
##                                    pot_dct = create_potential_in_crm(tag_dct)
##                                    print "POTENTIAL ADDED : ", pot_dct
                                
                    elif(entity_name == 'Contact'):
                        cust_up_dct = update_customer_in_crm(tag_dct, crm_id)
                        print "CUSTOMER UPDATED : ", cust_up_dct
                        contact_dct = create_contact_in_crm(tag_dct, crm_id)
                        print "CONTACT ADDED : ", contact_dct
                    
                    elif(entity_name == 'Product'):
                        if crm_id:
                            product_dct = update_product_in_crm(tag_dct, crm_id)
                            print "PRODUCT UPDATED : ", product_dct
                        else:
                            product_dct = create_product_in_crm(tag_dct)
                            print "PRODUCT ADDED : ", product_dct
                        
                    elif(entity_name == 'SalesOrder'):
                        if crm_id:
                            salesorder_dct = update_salesorder_in_crm(tag_dct, crm_id)
                            print "SALESORDER UPDATED : ", salesorder_dct
                        else:
                            salesorder_dct = create_salesorder_in_crm(tag_dct)
                            print "SALESORDER ADDED : ", salesorder_dct
                                             
                    #print "Record Created Successfully in CRM : ", tag_dct
        #return "All Customer Creation Success"
    except Exception,e:
        return "Error in Parsing : " + str(e)
    
def get_customer_from_qb(qb_id=None, acc_id=None, full_name=None):

    list_id_tag = ''
    if qb_id:
        list_id_tag = '<ListID>%s</ListID>' %(qb_id)

    if full_name:
        list_id_tag = '<FullName>%s</FullName>' %(full_name)
    
    qbxml_query = """<?xml version="1.0" encoding="utf-8"?>
                        <?qbxml version="13.0"?>
                        <QBXML>
                         <QBXMLMsgsRq onError="stopOnError">
                             <CustomerQueryRq>%s</CustomerQueryRq>
                        </QBXMLMsgsRq>
                       </QBXML>""" % (list_id_tag)
    resp_xml = quick_book_connector(qbxml_query)
    get_data_from_xml(resp_xml.encode('utf-8'), 'Customer', crm_id=acc_id)
    
def get_contact_from_qb(qb_id, acc_id):
    
    qbxml_query = """<?xml version="1.0" encoding="utf-8"?>
                        <?qbxml version="13.0"?>
                        <QBXML>
                         <QBXMLMsgsRq onError="stopOnError">
                             <CustomerQueryRq>
                             <ListID>%s</ListID>
                             </CustomerQueryRq>
                        </QBXMLMsgsRq>
                       </QBXML>""" % (qb_id)
    resp_xml = quick_book_connector(qbxml_query)
    get_data_from_xml(resp_xml.encode('utf-8'), 'Contact', crm_id=acc_id)
    
def get_salesorder_from_qb(txn_id=None, sale_id=None, ref_num=None):

    list_id_tag = ''
    if txn_id:
        list_id_tag = '<TxnID>%s</TxnID>' %(txn_id)

    if ref_num:
        list_id_tag = '<RefNumber>%s</RefNumber>' %(ref_num)
    
    qbxml_query = """<?xml version="1.0" encoding="utf-8"?>
                        <?qbxml version="13.0"?>
                        <QBXML>
                         <QBXMLMsgsRq onError="stopOnError">
                             <SalesOrderQueryRq>
                                 %s
                                 <IncludeLineItems>1</IncludeLineItems>
                             </SalesOrderQueryRq>
                        </QBXMLMsgsRq>
                       </QBXML>""" % (list_id_tag)
    resp_xml = quick_book_connector(qbxml_query)
    get_data_from_xml(resp_xml.encode('utf-8'), 'SalesOrder', crm_id=sale_id)
    
def get_item_from_qb(qb_id=None, product_id=None, full_name=None):

    list_id_tag = ''
    if qb_id:
        list_id_tag = '<ListID>%s</ListID>' %(qb_id)

    if full_name:
        list_id_tag = '<FullName>%s</FullName >' %(full_name)
    
    qbxml_query = """<?xml version="1.0" encoding="utf-8"?>
                        <?qbxml version="13.0"?>
                        <QBXML>
                         <QBXMLMsgsRq onError="stopOnError">
                             <ItemQueryRq>%s</ItemQueryRq>
                        </QBXMLMsgsRq>
                       </QBXML>""" % (list_id_tag)
    resp_xml = quick_book_connector(qbxml_query)
    get_data_from_xml(resp_xml.encode('utf-8'), 'Product', crm_id=product_id)
    
def update_customer_in_crm(tag_dct, account_id):

    acc_data = get_retrieve(account_id)
    acc_data['website'] = tag_dct.get('Website','')
    acc_data['email2'] = tag_dct.get('CC Email','')
    acc_data['otherphone'] = tag_dct.get('Work Phone','')
    acc_data['cf_809'] = tag_dct.get('ListID','')
    acc_data['cf_813'] = tag_dct.get('EditSequence','')
    return get_update("Accounts", acc_data)
    
def create_customer_in_crm(tag_dct):
    
    bill_code = tag_dct.get('BillAddress', {}).get('PostalCode','')
    bill_pobox = bill_code
    bill_street = tag_dct.get('BillAddress', {}).get('Addr1','')
    bill_city = tag_dct.get('BillAddress', {}).get('City','')
    bill_state = tag_dct.get('BillAddress', {}).get('State','')
    bill_country = tag_dct.get('BillAddress', {}).get('Country','')
     
    customer_dct = {'bill_code': bill_code,
                    'bill_pobox': bill_pobox,
                    'bill_street': bill_street, 
                    'bill_city': bill_city,
                    'bill_state': bill_state,
                    'bill_country': bill_country,
                    'ship_pobox': tag_dct.get('ShipAddress', bill_code),
                    'ship_code': tag_dct.get('ShipAddress', bill_pobox),
                    'ship_street': tag_dct.get('ShipAddress', bill_street), 
                    'ship_city': tag_dct.get('ShipAddress', bill_city),
                    'ship_state': tag_dct.get('ShipAddress', bill_state),
                    'ship_country': tag_dct.get('ShipAddress', bill_country), 
                    'accountname': tag_dct.get('Name',''), 
                    'website': tag_dct.get('Website',''), 
                    'fax': tag_dct.get('Fax',''), 
                    'email1': tag_dct.get('Email',''),
                    'phone': tag_dct.get('Phone',''), 
                    'cf_809': tag_dct.get('ListID',''),
                    'cf_813': tag_dct.get('EditSequence',''),
                    'description': tag_dct.get('Notes',''), 
                    'rating': 'Active',
                    'assigned_user_id': assigned_user_id,
                    #'modifiedby': assigned_user_id,
                    #'created_user_id': assigned_user_id,
                    'emailoptout': '0', 
                    'notify_owner': '0', 
                    'cf_823': '0', 
                    'isconvertedfromlead': '0', 
                    'siccode': '', 
                    'cf_825': '', 
                    'email2': '', 
                    'accounttype': '', 
                    'otherphone': '', 
                    'cf_811': '', 
                    'ownership': '', 
                    'employees': '', 
                    'annual_revenue': '', 
                    'cf_827': '', 
                    'industry': '', 
                    'tickersymbol': ''}
    
    return get_create('Accounts', customer_dct)

def create_contact_in_crm(tag_dct, account_id):
    
    contact_dct = { 'mailingstreet': tag_dct.get('BillAddress', {}).get('Addr1',''),
                    'mailingcity': tag_dct.get('BillAddress', {}).get('City',''),
                    'mailingzip': tag_dct.get('BillAddress', {}).get('PostalCode',''),
                    'mailingstate': tag_dct.get('BillAddress', {}).get('State',''),
                    'mailingpobox': tag_dct.get('BillAddress', {}).get('PostalCode',''),
                    'mailingcountry': tag_dct.get('BillAddress', {}).get('Country',''),
                    'otherstreet': tag_dct.get('ShipAddress', {}).get('Addr1',''),
                    'othercity': tag_dct.get('ShipAddress', {}).get('City',''),
                    'otherzip': tag_dct.get('ShipAddress', {}).get('PostalCode',''), 
                    'otherpobox': tag_dct.get('ShipAddress', {}).get('PostalCode',''),
                    'otherstate': tag_dct.get('ShipAddress', {}).get('State',''),
                    'othercountry': tag_dct.get('ShipAddress', {}).get('Country',''),
                    'otherphone': tag_dct.get('Work Phone',''),
                    'homephone': tag_dct.get('Main Phone',''),
                    'mobile': tag_dct.get('Mobile',''),
                    'phone': tag_dct.get('Phone',''), 
                    'email': tag_dct.get('Email',''),
                    'secondaryemail': tag_dct.get('CC Email',''), 
                    'fax': tag_dct.get('Fax',''), 
                    'account_id': account_id,
                    'salutationtype': tag_dct.get('Salutation',''), 
                    'firstname': tag_dct.get('FirstName',''), 
                    'lastname': tag_dct.get('LastName',''),
                    'description': tag_dct.get('Notes',''),
                    'title': tag_dct.get('JobTitle',''),
                    #'created_user_id': assigned_user_id, 
                    'assigned_user_id': assigned_user_id,
                    #'modifiedby': assigned_user_id,
                    'leadsource': 'Cold Call',  
                    'reference': '0',
                    'isconvertedfromlead': '0',
                    'emailoptout': '0',
                    'donotcall': '0',
                    'portal': '0',
                    'notify_owner': '0',
                    'cf_829': '', 
                    'support_start_date': '', 
                    'support_end_date': '', 
                    'assistantphone': '', 
                    'assistant': '', 
                    'department': '', 
                    'imagename': '', 
                    'birthday': '', 
                    'cf_831': '', }
    return get_create('Contacts', contact_dct)

def create_potential_in_crm(tag_dct):
    
    close_date = datetime.strftime(datetime.today()+ timedelta(days=30), "%Y-%m-%d %H:%M:%S")
    relative_contact_data = get_relative_contact(tag_dct['ParentRef']['ListID'])
    if relative_contact_data:
        potential_dct = {'potentialname': tag_dct.get('Name',''), 
                        'probability': '0.000', 
                        'related_to': '11x'+str(int(relative_contact_data[0][0])),
                        'contact_id': '12x'+str(int(relative_contact_data[0][1])),
                        'assigned_user_id': assigned_user_id, 
                        'leadsource': 'Cold Call',
                        'sales_stage': 'Prospecting',
                        #'modifiedby': assigned_user_id, 
                        #'created_user_id': assigned_user_id,
                        'isconvertedfromlead': '0',
                        'forecast_amount': '0.00000000',
                        'amount': '0.00000000',
                        'closingdate': close_date, 
                        'opportunity_type': '', 
                        'description': '', 
                        'cf_833': '',  
                        'cf_835': '', 
                        'cf_837': '',
                        'cf_894': tag_dct.get('ListID',''),
                        'cf_896': tag_dct.get('EditSequence',''),
                        'campaignid': '', 
                        'nextstep': ''}
        return get_create('Potentials', potential_dct)
    else:
        return {"error":"MISSING RELATIVE CONTACT"}

def create_product_in_crm(tag_dct):
    
    start_date = datetime.strftime(datetime.today(), "%Y-%m-%d %H:%M:%S")
    end_date = datetime.strftime(datetime.today()+ timedelta(days=30), "%Y-%m-%d %H:%M:%S")

    if (tag_dct.get('SalesOrPurchase',{}).get('AccountRef',{}).get('FullName','') == 'Income'):

        product_dct = { 'productname': tag_dct.get('SalesOrPurchase',{}).get('Desc',tag_dct.get('Name','')),
                        'productcode': tag_dct.get('Name',''),
                        'unit_price': tag_dct.get('SalesOrPurchase',{}).get('Price','0.00'),
                        'start_date': start_date,
                        'sales_start_date': start_date,
                        'sales_end_date': end_date,
                        'cf_847': tag_dct.get('ListID',''), 
                        'cf_849': tag_dct.get('EditSequence',''),
                        'cf_851': '',
                        'cf_960': 'Yes',
                        'description': tag_dct.get('SalesOrPurchase',{}).get('Desc',''),
                        #'created_user_id': assigned_user_id,
                        'assigned_user_id': assigned_user_id, 
                        #'modifiedby': assigned_user_id,
                        'commissionrate': '0.000',
                        'qtyindemand': '0', 
                        'reorderlevel': '0',
                        'productcategory': 'Hardware',
                        'qty_per_unit': '0.00',
                        'qtyinstock': '1000', 
                        'discontinued': '1',
                        'taxclass': '', 
                        'serial_no': '', 
                        'imagename': '', 
                        'vendor_part_no': '', 
                        'website': '', 
                        'mfr_part_no': '', 
                        'vendor_id': '', 
                        'expiry_date': '', 
                        'usageunit': '', 
                        'productsheet': '', 
                        'manufacturer': '', 
                        'glacct': '', 
                        }
        return get_create('Products', product_dct)
    else:
        return tag_dct.get('Name','')+ " is not a valid product!!!"

def update_product_in_crm(tag_dct, product_id):
    
    prd_data = get_retrieve(crm_module_mapping['product']['tab_id']+str(int(product_id)))
    #prd_data['productname'] = tag_dct.get('SalesOrPurchase',{}).get('Desc','')
    prd_data['cf_847'] = tag_dct.get('ListID','')
    prd_data['cf_849'] = tag_dct.get('EditSequence','')
    prd_data['cf_960'] = 'Yes'
    return get_update("Products", prd_data)

def create_salesorder_in_crm(tag_dct):
        
    bill_code = tag_dct.get('BillAddress', {}).get('Addr2','')
    bill_pobox = bill_code
    bill_city = tag_dct.get('BillAddress', {}).get('City','')
    bill_state = tag_dct.get('BillAddress', {}).get('State','')
    bill_country = tag_dct.get('BillAddress', {}).get('Country','')
    
    start_date = datetime.strftime(datetime.today(), "%Y-%m-%d %H:%M:%S")
    end_date = datetime.strftime(datetime.today()+ timedelta(days=30), "%Y-%m-%d %H:%M:%S")
    
    relative_potential_data = get_relative_potential(tag_dct['CustomerRef']['ListID'])
    if relative_potential_data:
        relative_prod_data = get_relative_product(tag_dct['SalesOrderLineRet']['ItemRef']['ListID'])
        street_address = relative_potential_data[0][3] if relative_potential_data[0][3] else "Billing Address"
        bill_street = tag_dct.get('BillAddress', {}).get('Addr1', street_address)
        salesorder_dct = {
                        'bill_street': bill_street,
                        'bill_pobox': bill_pobox,
                        'bill_code': bill_code,
                        'bill_city': bill_city,
                        'bill_state': bill_state,
                        'bill_country': bill_country, 
                        
                        'ship_street': tag_dct.get('ShipAddress', bill_street),
                        'ship_pobox': tag_dct.get('ShipAddress', bill_pobox),
                        'ship_code': tag_dct.get('ShipAddress', bill_code),
                        'ship_city': tag_dct.get('ShipAddress', bill_city), 
                        'ship_country': tag_dct.get('ShipAddress', bill_country),
                        'ship_state': tag_dct.get('ShipAddress', bill_state),
                        
                        'account_id': crm_module_mapping['account']['tab_id']+str(int(relative_potential_data[0][0])),
                        'contact_id': crm_module_mapping['contact']['tab_id']+str(int(relative_potential_data[0][1])),
                        'potential_id': crm_module_mapping['potential']['tab_id']+str(int(relative_potential_data[0][2])),
                        'assigned_user_id': assigned_user_id,
                        #'modifiedby': assigned_user_id,
                        'currency_id': currency_id,
                        'quote_id': '',
                        'productid': crm_module_mapping['product']['tab_id']+str(int(relative_prod_data[0][0])),
                        
                        'subject': tag_dct.get('SalesOrderLineRet',{}).get('ItemRef',{}).get('FullName',''),
                        'start_period': '',
                        'duedate': end_date,
                        'sostatus': 'Created', 
                        'description': tag_dct.get('SalesOrderLineRet',{}).get('Desc',''),
                        'terms_conditions': '- Unless otherwise agreed in writing by the supplier all invoices are payable within thirty (30) days of the date of invoice, in the currency of the invoice, drawn on a bank based in India or by such other method as is agreed in advance by the Supplier.\n\n - All prices are not inclusive of VAT which shall be payable in addition by the Customer at the applicable rate.',
                        'carrier': 'FedEx', 
                        'enable_recurring': '0',
                        
                        'cf_815': '', 
                        'cf_817': tag_dct.get('TxnID',''), 
                        'cf_819': tag_dct.get('EditSequence',''),
                        'cf_821': tag_dct.get('SalesOrderLineRet',{}).get('TxnLineID',''), 
                        'cf_839': '', 
                        'cf_878': '0', 
                        'cf_884': 'Onnet',
                        'cf_890': '', 
                        'cf_892': 'Open', 
                        
                        'LineItems': [{'comment': '', 
                                'incrementondel': '0', 
                                'tax2': '0.000',
                                'description': tag_dct.get('SalesOrderLineRet',{}).get('Desc',''), 
                                'discount_amount': '', 
                                'discount_percent': '', 
                                'tax1': '0.000', 
                                'tax3': '0.000', 
                                'productid': crm_module_mapping['product']['tab_id']+str(int(relative_prod_data[0][0])),
                                'listprice': str(relative_prod_data[0][1]), 
                                'quantity': str(relative_prod_data[0][2])
                                }], 
                        }
        return get_create('SalesOrder', salesorder_dct)
    else:
        return {"error":"MISSING RELATIVE OPPORTUNITY"}

def update_potential_in_crm(tag_dct, pot_id):
    
    pot_data = get_retrieve(pot_id)
    pot_data['cf_894'] = tag_dct.get('ListID','')
    pot_data['cf_896'] = tag_dct.get('EditSequence','')
    return get_update("Potential", pot_data)

def fetch_missing_potential():
    potentials = get_missing_potential()
    for pot in potentials:
        truncated_potential_name = pot[3]+":"+get_truncated_name(pot[1], pot[0])
        pot_id = str(int(pot[2]))
        pot_up_status = get_customer_from_qb(acc_id=crm_module_mapping['potential']['tab_id']+pot_id, full_name=truncated_potential_name)
        if pot_up_status == None:
            pot_up_status = get_customer_from_qb(acc_id=crm_module_mapping['potential']['tab_id']+pot_id, full_name=truncated_potential_name.split('-')[0]+'--')
        print pot_up_status

def update_salesorder_in_crm(tag_dct, salesorder_id):

    from db_connector import get_db_query
    sale_id = salesorder_id.replace(crm_module_mapping['salesorder']['tab_id'],'')
    sql_query = "UPDATE vtiger_salesordercf set cf_817 = '%s', cf_819 = '%s', cf_821 = '%s' where salesorderid = %s;" % (tag_dct.get('TxnID',''),
                    tag_dct.get('EditSequence',''), tag_dct.get('SalesOrderLineRet',{}).get('TxnLineID',''), sale_id)
    return get_db_query(sql_query, 'update')

def fetch_missing_salesorder():
    sales = get_missing_salesorder()
    for sale in sales:
        sale_id = str(int(sale[0]))
        sale_resp = get_salesorder_from_qb(sale_id=crm_module_mapping['salesorder']['tab_id']+sale_id, ref_num=crm_module_mapping['salesorder']['tab_id']+sale_id)
        if not sale_resp:
            sale_resp = get_salesorder_from_qb(sale_id=crm_module_mapping['salesorder']['tab_id']+sale_id, ref_num=sale_id)
        print sale_resp

def fetch_missing_account():
    accounts = get_missing_account()
    for acc in accounts:
        acc_id = str(int(acc[1]))
        print get_customer_from_qb(acc_id=crm_module_mapping['account']['tab_id']+acc_id, full_name=acc[0])

def add_new_product_int_qb():
    products = get_missing_product()
    for prod in products:
        prod_id = str(int(prod[0]))
        productid = crm_module_mapping['product']['tab_id'] + prod_id.strip().replace(crm_module_mapping['product']['tab_id'],'')
        data = {'id':productid, 'productcode':prod[2], 'productname':prod[1], 'unit_price':prod[3], 'productcategory':prod[4]}
        xml_req = get_product_new_xml(data, prod_id)
        qbxml_query = generate_qbxml_query(xml_req)
        resp_xml = quick_book_connector(qbxml_query)
        print resp_xml
        get_data_from_xml(resp_xml.encode('utf-8'), 'Product', crm_id=prod_id)

def update_existing_product_into_crm():
    products = get_missing_product()
    for prod in products:
        prod_id = str(int(prod[0]))
        print prod_id
        get_item_from_qb(product_id=prod_id, full_name=prod[2].strip())

       
if __name__ == '__main__':

    import sys
    try:
        module = sys.argv[1]
    except:
        module = 'nomodule'
        
    if module == 'product':
        get_item_from_qb()
    elif module == 'account':
        get_customer_from_qb()
    else:
        print "Please mention the module to integrate !!!"

    fetch_missing_potential()
    

