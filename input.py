#!/usr/bin/env python
""" 
==============================================================================
title           : input.py
description     : This is just a test file to store the inputs to the QuickBooks SDK
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python input.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

'''
#     entity_type = "Invoice"
#     base_tag = "InvoiceRet"
#     filename = 'C:\Users\Binny.abraham\Documents\scripts\QuickBookInvoice.csv'
#     csv_writer = get_csv_writer(filename)
#     crm_data = get_data(entity_type)
#     quick_book(csv_writer, crm_data, entity_type, base_tag)

    tag_dct = {"roottag":{"tagname":"CustomerAddRq","id":1},"basetag":{"tagname":"CustomerAdd","id":1},
               "tags":{"Name":{"value":"BBB Comp","order":1},"IsActive":{"value":"1","order":2},\
                "CompanyName":{"value":"BBB Company","order":3},"Salutation":{"value":"dsd","order":4},\
                "FirstName":{"value":"dsd","order":5},"LastName":{"value":"dsd","iorder":6},\
                "BillAddress":{"value":{"Addr1":{"value":"dsd","order":1},"City":{"value":"bangalore","order":2},\
                               "State":{"value":"Karnataka","order":3},"PostalCode":{"value":"12345","order":4},\
                               "Country":{"value":"india","order":5}},"order":7},\
                "ShipAddress":{"value":{"Addr1":{"value":"dsd","order":1},"City":{"value":"bangalore","order":2},\
                               "State":{"value":"Karnataka","order":3},"PostalCode":{"value":"12345","order":4},\
                               "Country":{"value":"india","order":5}},"order":7},\
                "Phone":{"value":12345,"order":9},"AltPhone":{"value":12345,"order":10},"Fax":{"value":12345,"order":11},\
                "Email":{"value":"aaa@gmail.com","order":12},"Cc":{"value":"bbb@gmail.com","order":13}}}
    xml = generate_req_xml(tag_dct)
    qbxml_query = generate_qbxml_query(xml)
    '''
    
qbxml_query = """
        <?qbxml version="13.0"?>
        <QBXML><!-- onError may be set to continueOnError or stopOnError. -->
        <QBXMLMsgsRq onError="stopOnError"><!-- Begin first request: adding a customer -->
          <InvoiceAddRq requestID="5">
            <InvoiceAdd defMacro="TxnID:1234">
              <CustomerRef>
                <ListID>800021EE-1409135586</ListID>
                <FullName>Rafik Shafik</FullName>
              </CustomerRef>
              <TxnDate>2014-08-29</TxnDate>
              <RefNumber>126-ABC</RefNumber>
              <InvoiceLineAdd>
                <ItemRef>
                  <ListID >80000012-1409147100</ListID> <!-- optional -->
                  <FullName>Internet</FullName>
                </ItemRef>
                <Desc>For the house</Desc>
                <Quantity>1</Quantity>
                <Rate>10.00</Rate>
              </InvoiceLineAdd>
            </InvoiceAdd>
          </InvoiceAddRq>
          </QBXMLMsgsRq>
        </QBXML>
    """

qbxml_sale_query = """
    <?qbxml version="13.0"?>
        <QBXML>
            <QBXMLMsgsRq onError="stopOnError">
                <SalesOrderAddRq requestID="6">
                    <SalesOrderAdd defMacro="TxnID:A123"> <!-- required -->
                        <CustomerRef> <!-- required -->
                            <ListID>800021EE-1409135586</ListID> <!-- optional -->
                            <FullName>Rafik Shafik</FullName> <!-- optional -->
                        </CustomerRef>
                        <TxnDate>2014-08-27</TxnDate> <!-- optional -->
                        <RefNumber>125-ABC</RefNumber> <!-- optional -->
                        <Other>i am binny</Other> <!-- optional -->
                        <SalesOrderLineAdd> <!-- optional -->
                            <ItemRef> <!-- optional -->
                                <ListID>80000012-1409147100</ListID> <!-- optional -->
                                <FullName>Internet</FullName> <!-- optional -->
                            </ItemRef>
                            <Desc>For the house</Desc> <!-- optional -->
                            <Quantity>1</Quantity> <!-- optional -->
                            <Rate>10.00</Rate> <!-- optional -->
                            <Amount >20.00</Amount> <!-- optional -->
                        </SalesOrderLineAdd>
                    </SalesOrderAdd>
                </SalesOrderAddRq>
            </QBXMLMsgsRq>
        </QBXML>
    """
    
qbxml_service_query = """
        <?qbxml version="13.0"?>
        <QBXML>
            <QBXMLMsgsRq onError="continueOnError">
                <ItemServiceAddRq>
                    <ItemServiceAdd>
                        <Name>Internet</Name>
                        <SalesOrPurchase>
                            <Desc>Clean gutter</Desc>
                            <Price>45.67</Price>
                            <AccountRef>
                                <FullName>Payroll Liabilities</FullName>
                            </AccountRef>
                        </SalesOrPurchase>
                    </ItemServiceAdd>
                </ItemServiceAddRq>
            </QBXMLMsgsRq>
        </QBXML>
    """

    
#print quick_book_connector(qbxml_sale_query)
