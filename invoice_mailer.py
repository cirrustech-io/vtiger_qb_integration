#!/usr/bin/env python
""" 
==============================================================================
title           : invoice_mailer.py
description     : This file is used to send the attachment mails 
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python invoice_mailer.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import smtplib
# For guessing MIME type
import mimetypes
# Import the email modules we'll need
import email
import email.mime.application
from qbcrm_config import smtp_host, smtp_port, sender, invoice_path, default_mail_lst


def attachment_mailer(product_name, invoice_num, reciever_mail):

    reciever = default_mail_lst['it_department']
    reciever.append(reciever_mail) 
  
    # Create a text/plain message
    msg = email.mime.Multipart.MIMEMultipart()
    msg['Subject'] = 'Your invoice for %s - %s' % (product_name, invoice_num)
    msg['From'] = sender
    msg['To'] = reciever[-1]

    # The main body is just another attachment
    body = email.mime.Text.MIMEText("""Your Invoice is attached. Please go through the attachment.""")
    msg.attach(body)

    # PDF attachment
    filename = generate_invoice_file_path(product_name, invoice_num, '.pdf')
    fp=open(filename,'rb')
    att = email.mime.application.MIMEApplication(fp.read(),_subtype="pdf")
    fp.close()
    att.add_header('Content-Disposition','attachment',filename=filename)
    msg.attach(att)

    # Send mail. I use the default port 25, but I authenticate. 
    s = smtplib.SMTP(smtp_host, smtp_port)
    s.sendmail(sender, reciever, msg.as_string())
    s.quit()

def generate_invoice_file_path(product_name, invoice_num, file_ext):
    return invoice_path + invoice_num.replace(' ','_').strip() + '_' +product_name.replace(' ','_').strip() + file_ext

