from db_connector import get_db_query
import csv


def get_id_from_salesorder():

    sql_query = "select sale.salesorderid from vtiger_salesorder sale, vtiger_crmentity crm where sale.sostatus = 'Delivered' and crm.deleted = 0 and crm.crmid = sale.salesorderid and sale.salesorderid NOT IN (141479, 141509, 143494)"
    ret_data = get_db_query(sql_query, 'select')
    for sale in ret_data:
        in_query = "INSERT INTO vtiger_invoice_recurring_info VALUES(%s, 'Daily', '2016-07-01', '2016-10-01', NULL, 'Net 30 days', 'AutoCreated');" % (sale[0])
        print get_db_query(in_query, 'insert')

def update_product(prod_code, rec_val, web_cdr):
    up_qry = "UPDATE vtiger_products prd, vtiger_productcf pcf SET pcf.cf_1049 = %s, pcf.cf_1051 = %s WHERE prd.productcode = '%s' AND prd.productid = pcf.productid;" % (rec_val, web_cdr, prod_code)
    print up_qry
    #get_db_query(up_qry, 'update')

def get_product_value(val):
    prd_map_dct = {'Yes':1, 'No':0}
    return prd_map_dct[val]

if __name__ == '__main__':
    prd_file = 'docs/product_details.csv'
    with open(prd_file, 'rb') as pd_f:
        csv_rdr = csv.reader(pd_f)
        for row in csv_rdr:
            if row[1] != 'Product Code':
                update_product(row[1].strip(), get_product_value(row[7]), get_product_value(row[8]))
    

