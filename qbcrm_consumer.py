#!/usr/bin/env python
""" 
==============================================================================
title           : qbcrm_consumer.py
description     : This is rabbitmq consumer file which consumes messages from rabbitmq
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python qbcrm_consumer.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

from rmqTask import Task
from rmqConfig import QUEUE_MAP, MAIN_EXCHANGE
task = Task()
import sys

try:
    queue_map_name = str(sys.argv[1])
except:
    queue_map_name = 'workflow_new_account'
    
queue_name = QUEUE_MAP[queue_map_name]['queue_name']
routing_key = QUEUE_MAP[queue_map_name]['routing_key']
exchange_name = MAIN_EXCHANGE['name']

try:
    task.consume_message(exchange_name, queue_name, routing_key)
    task.close_connection()
except Exception,e:
    print e
