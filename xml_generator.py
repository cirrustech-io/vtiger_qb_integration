#!/usr/bin/env python
""" 
==============================================================================
title           : xml_generator.py
description     : This file is used to generate xml using yattag module
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python xml_generator.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

from yattag import Doc
from lxml import etree


def Yat_test():
    doc, tag, text = Doc().tagtext()

    with tag('food'):
        with tag('name'):
            text('French Breakfast')
        with tag('price', currency='USD'):
            text('6.95')
        with tag('ingredients'):
            for ingredient in ('baguettes', 'jam', 'butter', 'croissants'):
                with tag('ingredient'):
                    text(ingredient)
    print(doc.getvalue())

def Element_test(text):
    import xml.etree.ElementTree as ET
    root = ET.fromstring(text)
    print root.tag
    print root.text

    for child in root:
        for chd in child:
            print chd.tag, chd.text

def xml_dict(text):
    import xmltodict, json
    result = xmltodict.parse(text)
    print result
    print json.dumps(result) 

if __name__ == "__main__":
    
    text = """<?xml version="1.0"?>
<data>
    <country name="Liechtenstein">
        <rank>1</rank>
        <year>2008</year>
        <gdppc>141100</gdppc>
        <neighbor name="Austria" direction="E"/>
        <neighbor name="Switzerland" direction="W"/>
    </country>
    <country name="Singapore">
        <rank>4</rank>
        <year>2011</year>
        <gdppc>59900</gdppc>
        <neighbor name="Malaysia" direction="N"/>
    </country>
    <country name="Panama">
        <rank>68</rank>
        <year>2011</year>
        <gdppc>13600</gdppc>
        <neighbor name="Costa Rica" direction="W"/>
        <neighbor name="Colombia" direction="E"/>
    </country>
</data>"""

    xml_dict(text)

    
