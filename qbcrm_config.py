#!/usr/bin/env python
""" 
==============================================================================
title           : qbcrm_config.py
description     : This is quickbooks and crm configuration file
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python qbcrm_config.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import os
qb_module_mapping = {'account':{'full_name_tag':'FullName', 'list_id_tag':'ListID', 'crm_id_tag':'Notes', 'base_tag':'CustomerRet', 
                                'edit_sequence_tag':'EditSequence', 'current_balance_tag':'Balance', 'query_req_tag':'CustomerQueryRq'},
                     'potential':{'full_name_tag':'FullName', 'list_id_tag':'ListID', 'crm_id_tag':'Notes', 'base_tag':'CustomerRet', 
                                'edit_sequence_tag':'EditSequence', 'current_balance_tag':'Balance', 'query_req_tag':'CustomerQueryRq'},
                     'product':{'full_name_tag':'FullName', 'list_id_tag':'ListID', 'crm_id_tag':'Desc', 'base_tag':'ItemServiceRet', 
                                'edit_sequence_tag':'EditSequence', 'query_req_tag':'ItemServiceQueryRq'},
                     'salesorder':{'full_name_tag':'RefNumber', 'list_id_tag':'TxnID', 'crm_id_tag':'RefNumber', 'base_tag':'SalesOrderRet', 
                                   'edit_sequence_tag':'EditSequence', 'query_req_tag':'SalesOrderQueryRq', 'line_id_tag':'SalesOrderLineRet', 
                                   'txn_line_id_tag':'TxnLineID'},
                     'invoice':{'full_name_tag':'RefNumber', 'list_id_tag':'TxnID', 'crm_id_tag':'Other', 'base_tag':'InvoiceRet', 
                                'edit_sequence_tag':'EditSequence', 'query_req_tag':'InvoiceQueryRq', 'line_id_tag':'InvoiceLineRet', 
                                'txn_line_id_tag':'TxnLineID'},
                     'creditmemo':{'full_name_tag':'RefNumber', 'list_id_tag':'TxnID', 'crm_id_tag':'Other', 'base_tag':'CreditMemoRet', 
                                'edit_sequence_tag':'EditSequence', 'query_req_tag':'CreditMemoQueryRq', 'line_id_tag':'CreditMemoLineRet', 
                                'txn_line_id_tag':'TxnLineID'},
                     }

crm_module_mapping = {'account':{'module':'Accounts', 'tab_id':'11x', 'quickbooks_id_tag':'cf_809', 'edit_sequence_tag':'cf_813', 'qb_status_tag':'cf_898', 'table_name':'vtiger_accountscf'},
                      'contact':{'module':'Contacts', 'tab_id':'12x', 'quickbooks_id_tag':'', 'edit_sequence_tag':'', 'qb_status_tag':'', 'table_name':'vtiger_contactscf'},
                      'potential':{'module':'Potentials', 'tab_id':'13x', 'quickbooks_id_tag':'cf_894', 'edit_sequence_tag':'cf_896', 'qb_status_tag':'cf_900', 'table_name':'vtiger_potentialscf', 'bill_frequency_tag':'cf_1015', 'bill_period_tag':'cf_1017', 'opp_group_tag':'cf_1043', 'circuit_id_tag':'cf_954',
                                  'billing_address':{'flat_tag':'cf_1019', 'road_tag':'cf_1023', 'postal_tag':'cf_1027', 'state_tag':'cf_1031','building_tag':'cf_1021', 'block_tag':'cf_1025', 'city_tag':'cf_1029', 'country_tag':'cf_1033'}},
                      'salesorder':{'module':'SalesOrder', 'tab_id':'6x', 'quickbooks_id_tag':'cf_817', 'edit_sequence_tag':'cf_819', 'txn_line_id_tag':'cf_821', 'qb_status_tag':'cf_892', 'table_name':'vtiger_salesordercf', 'commissioning_date_flag':'cf_998', 'invoice_start_date_tag':'cf_1000', 'trial_period_tag':'cf_1002', 'contract_expiry_date_tag':'cf_1004', 'sale_type_tag':'cf_1035', 'contract_term_tag':'cf_1037', 'invoice_stop_date_tag':'cf_1039', 'service_disconnection_date_tag':'cf_1041'},
                      'product':{'module':'Products', 'tab_id':'14x', 'quickbooks_id_tag':'cf_847', 'edit_sequence_tag':'cf_849', 'txn_line_id_tag':'cf_851', 'qb_status_tag':'cf_902', 'table_name':'vtiger_productcf'},
                      'invoice':{'module':'Invoice', 'tab_id':'7x', 'quickbooks_id_tag':'cf_841', 'edit_sequence_tag':'cf_843', 'txn_line_id_tag':'cf_845', 'qb_status_tag':'cf_906', 'table_name':'vtiger_invoicecf', 'reference_no_tag':'cf_1067', 'opp_group_tag':'cf_1069'},
                      'creditmemo':{'module':'Invoice', 'tab_id':'7x', 'quickbooks_id_tag':'cf_841', 'edit_sequence_tag':'cf_843', 'txn_line_id_tag':'cf_845', 'qb_status_tag':'cf_906', 'table_name':'vtiger_invoicecf', 'reference_no_tag':'cf_1067', 'opp_group_tag':'cf_1069'}
                      }

qb_app_name = 'QBSDKInfonasIntegration'
qb_company_file = os.path.join("D:\\", "QuickBooks", "QuickBooks15", "Infonas WLL", "Infonas WLL")
qb_data_file = os.path.join("D:\\", "QuickBooks", "QuickBooks15", "Infonas WLL", "Infonas WLL.qbw")
qb_user_mode = {'multi_user':1, 'single_user':0}

qb_logger_name = 'qbsdk'
if os.name == 'posix':
    qb_logger_path = '/home/binny/infonas/vtiger_qb_integration/qbsdk.log'
else:
    qb_logger_path = os.path.join("D:\\", "Integration", "infonas", "vtiger_qb_integration", "qbsdk.log")


#Vtiger Credentials
vtiger_webservice_url = 'https://crm.i.infonas.com/webservice.php'
vtiger_user_name = 'admin'
vtiger_user_token = "cVDOx0Qg8KH9nMrC"
assigned_user_id = "19x25"
currency_id = "21x1"
site_url = 'https://crm.i.infonas.com/'

smtp_host = 'mail.i.infonas.com'
smtp_port = 25
sender = 'crm@i.infonas.com'
invoice_path = './invoice/'

default_mail_lst = {'it_department':['binny.abraham@infonas.com']}
