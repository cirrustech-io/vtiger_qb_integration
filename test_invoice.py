
from vtiger_connect import get_retrieve, get_update, get_query
from invoice_generator import generate_invoice
from publisher import publish_to_queue
from db_connector import get_db_query
from qbcrm_config import qb_module_mapping, crm_module_mapping
from qbcrm_helper import get_potential_group, inv_opp_group_key, opp_group_key
from create_invoice_from_so import create_Invoice_From_SO

def Invoice_PDF(data, routing_key):
    #publish_to_queue(data, 'InvoiceEmail')
    invoice_pdf = generate_invoice(data, routing_key)

def generate_invoice_from_opportunity_group(invoice_date):
    
    opp_group_data = get_potential_group()
    for opp_group in opp_group_data:
        pot_query = "SELECT potentialid FROM vtiger_potentialscf WHERE %s = %s;" % (opp_group_key, opp_group[0])
        pot_data = get_db_query(pot_query, 'select')
        for pot in pot_data:
            sale_query = "SELECT sale.salesorderid FROM vtiger_salesorder sale, vtiger_crmentity crm WHERE sale.salesorderid = crm.crmid AND sale.sostatus = 'Delivered' AND crm.deleted=0 AND sale.potentialid = %s" % (pot[0])
            sale_data = get_db_query(sale_query, 'select')
            if sale_data:
                sale_data[0][0]
                #create_Invoice_From_SO(sale_data[0][0], invoice_date)

        generate_invoice_pdf(invoice_date, opp_group[0])

def generate_invoice_pdf(invoice_date, opp_group_id):
    data = {'payload':{'account':'', 'invoice':[]}}
    
    inv_query = "SELECT inv.invoiceid FROM vtiger_invoice inv, vtiger_invoicecf invcf, vtiger_crmentity crm WHERE invcf.%s = %s AND invcf.%s!='' AND inv.invoiceid = invcf.invoiceid AND crm.crmid = inv.invoiceid AND crm.deleted = 0 AND inv.invoicedate = '%s' AND inv.invoicestatus = 'AutoCreated';" % (inv_opp_group_key, opp_group_id, crm_module_mapping['invoice']['quickbooks_id_tag'], invoice_date)
            
    inv_data = get_db_query(inv_query, 'select')
    for inv_id in inv_data:
        data['payload']['invoice'].append(get_retrieve(crm_module_mapping['invoice']['tab_id']+str(inv_id[0])))

    if data['payload']['invoice']:
        data['payload']['account'] = get_retrieve(data['payload']['invoice'][0]['account_id'])
        #publish_to_queue(data, 'InvoicePDF')
        #Invoice_PDF(data, 'crm.email_invoice')


if __name__ == "__main__":

	import sys
        """
        ### GENERATE INVOICE PDF
	invoice_id = sys.argv[1]
        print invoice_id
	data = {"payload":{"account":{}, "salesorder":{}, "contact":{}, "product":{}, "invoice":{}}}
        print crm_module_mapping['invoice']['tab_id']+str(invoice_id)
	data['payload']['invoice'] = get_retrieve(crm_module_mapping['invoice']['tab_id']+str(invoice_id))
        print data
	print Invoice_PDF(data, 'crm.email_invoice')
        """
       
        ###GET INVOICE MONTHLY FROM OPPORTUNITY GROUP
        invoice_date = sys.argv[1]
        generate_invoice_from_opportunity_group(invoice_date)

        """        
        ### GENERATE INVOICE PDF FROM OPPORTUNITY GROUP
        invoice_date = sys.argv[1]
        generate_invoice_pdf(invoice_date)
        """

