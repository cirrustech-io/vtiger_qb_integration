
from db_connector import get_db_query
from datetime import datetime, timedelta
from qbcrm_config import crm_module_mapping
from calendar import monthrange

sale_type_key = crm_module_mapping['salesorder']['sale_type_tag']
invoice_start_date_key = crm_module_mapping['salesorder']['invoice_start_date_tag']
invoice_stop_date_key = crm_module_mapping['salesorder']['invoice_stop_date_tag']

def get_partial_amount(invoice_start_date, invoice_date, mrc_amount, credit_flag=False):
    
    partial_amount = 0.0

    inv_strt_dte = get_date(invoice_start_date, '%Y-%m-%d')
    inv_dte = get_date(invoice_date, '%Y-%m-%d')
    
    invoice_start_year = inv_strt_dte.year
    invoice_prev_year = get_prev_date(inv_dte, 5).year

    invoice_start_month = inv_strt_dte.month
    invoice_prev_month = get_prev_date(inv_dte, 5).month
    
    invoice_start_day = inv_strt_dte.day

    if((invoice_start_year == invoice_prev_year) and (invoice_start_month == invoice_prev_month) and (invoice_start_day!=1)):
        
        inv_calc_strt_day = invoice_start_day - 1
        total_days = get_total_days_from_month(invoice_start_year, invoice_start_month)[1]
        partial_days = total_days - inv_calc_strt_day
        partial_amount = partial_calculation(mrc_amount, total_days, partial_days)

    return float(round(partial_amount, 3))

def get_total_days_from_month(yr, mnth):
    return monthrange(yr, mnth)

def get_date(dte, formt):
    return datetime.strptime(dte, formt)

def get_prev_date(dte, dys):
    return (dte - timedelta(days=dys))
   
def partial_calculation(mrc_amount, total_days, partial_days):
    return ((float(mrc_amount)/int(total_days)) * int(partial_days))

