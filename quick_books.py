#!/usr/bin/env python
""" 
==============================================================================
title           : quick_books.py
description     : This is the quickbooks connector file. All quickbooks communication using quickbooks sdk is happening here
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python quick_books.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import win32com.client
import xml.etree.ElementTree
from yattag import Doc
import textwrap
import xmltodict
from datetime import datetime
from enum import Enum
import uuid
from qbcrm_config import *
from qbcrm_helper import inv_ref_no_key, remove_tabid, prod_qb_id_key

def quick_book_connector(qbxml_query):
    # Connect to Quickbooks
    sessionManager = win32com.client.Dispatch("QBXMLRP2.RequestProcessor")
    sessionManager.OpenConnection('', qb_app_name)
    ticket = sessionManager.BeginSession(qb_data_file, qb_user_mode['multi_user'])

    # Send query and receive response
    response_string = sessionManager.ProcessRequest(ticket, qbxml_query)

    # Disconnect from Quickbooks
    sessionManager.EndSession(ticket)     # Close the company file
    sessionManager.CloseConnection()      # Close the connection
    return response_string

def generate_qbxml_query(xml_req):
    # Generate qb xml request
      
    qbxml_query = """
        <?qbxml version="13.0"?>
        <QBXML><!-- onError may be set to continueOnError or stopOnError. -->
            <QBXMLMsgsRq onError="stopOnError"><!-- Begin first request: adding a customer -->
                %s
            </QBXMLMsgsRq>
        </QBXML>
    """ % (xml_req)
    return qbxml_query

def generate_req_xml(tag_dct):

    doc, tag, text = Doc().tagtext()
    with tag(tag_dct['roottag']['tagname'], requestID=str(tag_dct['roottag']['id'])):
        with tag(tag_dct['basetag']['tagname']):
            for val in sorted(tag_dct['tags'].items(), key=lambda x: x[1]):
                with tag(val[0]):
                    if type(val[1]['value']) == dict:
                        for ite in sorted(val[1]['value'].items(), key=lambda y: y[1]):
                            with tag(ite[0]):
                                text(str(ite[1]['value']))
                    else:
                        text(str(val[1]['value']))
    return doc.getvalue()

def parse_response_old(entity_type, response_string, base_tag, full_name_tag, list_id_tag, crm_id_tag):

    # Parse the response into an Element Tree and peel away the layers of response
    
    QBXML = xml.etree.ElementTree.fromstring(response_string)
    QBXMLMsgsRs = QBXML.find('QBXMLMsgsRs')
    QueryResponse = QBXMLMsgsRs.getiterator(base_tag)

    fullname = ''
    listid = ''
    crmid = ''
    
    if QueryResponse:
        QueryRet = QueryResponse[0]
        try:
            fullname = QueryRet.find(full_name_tag).text
        except:
            fullname = ''
        try:
            listid = QueryRet.find(list_id_tag).text
        except:
            listid = ''
        try:
            crmid = QueryRet.find(crm_id_tag).text
        except:
            crmid = QueryRet.find('SalesOrPurchase').find(crm_id_tag).text
    
    return fullname, listid, crmid

    """
    # Parse the response into an Element Tree and peel away the layers of response
    QBXML = xml.etree.ElementTree.fromstring(response_string)
    QBXMLMsgsRs = QBXML.find('QBXMLMsgsRs')
    InventoryAdjustmentQueryRs = QBXMLMsgsRs.getiterator("InventoryAdjustmentRet")
    for InvAdjRet in InventoryAdjustmentQueryRs:
        txnid = InvAdjRet.find('TxnID').text
        memo = InvAdjRet.find('memo').text
        print txnid
        print memo
    """
    
def parse_response(entity_type, response_string, base_tag, tag_to_parse):

    # Parse the response into an Element Tree and peel away the layers of response
    
    QBXML = xml.etree.ElementTree.fromstring(response_string)
    QBXMLMsgsRs = QBXML.find('QBXMLMsgsRs')

    if(base_tag == None):
        return QBXMLMsgsRs[0].attrib
        
    QueryResponse = QBXMLMsgsRs.getiterator(base_tag)

    ret_value = ''
    if QueryResponse:
        QueryRet = QueryResponse[0]
        if(tag_to_parse == 'TxnLineID'):
            line_id_tag = qb_module_mapping[entity_type]['line_id_tag']
            line_id_list = QueryRet.findall(line_id_tag)
            ret_lst = []
            for line_id in line_id_list:
                line_dct = {'qb_txn_line_id':line_id.find(tag_to_parse).text, 'qb_txn_product_id':line_id.find('ItemRef').find('ListID').text, 
                            'crm_lineitem_id':line_id.find('Other1').text, 'crm_sequence_no':line_id.find('Other2').text}
                ret_lst.append(line_dct)
            ret_value = ret_lst
        else:
            try:
                ret_value = QueryRet.find(tag_to_parse).text
            except:
                ret_value = QueryRet.find('SalesOrPurchase').find(tag_to_parse).text.split(":::")[0].strip() 
    return ret_value

def xml_to_dict(text):
    result = xmltodict.parse(text)
    return result

def format_qb_data(entity_type, action, data):

    relative_dct = None
    try:
        entity_dct = data['payload'][entity_type]
    except:
        entity_dct = data['payload']['invoice']
        
    if(entity_type=='potential'):
        relative_dct = data['payload']['account']
        
    if(entity_type=='account' or entity_type=='potential'):
        entity_dct['contact'] = data['payload'].get('contact')
    fun_name = "get_"+entity_type+"_"+action+"_xml"
    tag_dct = eval(fun_name)(entity_dct, relative_dct)
    if(entity_type=='account' or entity_type=='potential'):
        xml = generate_req_xml(tag_dct)
    else:
        xml = tag_dct
    qbxml_query = generate_qbxml_query(xml)
    return qbxml_query

def refine_data(data):
    if data:
        return str(data)
    else:
        return ""

def wrap_the_text(text, indx, chars=None):
    no_of_chars = 12
    if chars:
        no_of_chars = chars 
   
    try:
        ret_text = textwrap.wrap(text, no_of_chars)[indx]
    except:
        ret_text = ""
    return ret_text

def get_truncated_name(record_no, record_name, chars=None):
    no_of_chars = 30
    if chars:
        no_of_chars = chars
    return record_no + '-' + wrap_the_text(record_name, 0, no_of_chars)
    
def get_account_new_xml(data, relative_data=None):
    
    truncated_account_name = get_truncated_name(data['account_no'], data['accountname'])
    
    tag_dct = {"roottag":{"tagname":"CustomerAddRq","id":1},"basetag":{"tagname":"CustomerAdd","id":1},
               "tags":{"Name":{"value":data['accountname'],"order":1},"IsActive":{"value":"1","order":2},\
                "CompanyName":{"value":data['accountname'],"order":3},"Salutation":{"value":"","order":4},\
                "FirstName":{"value":"","order":5},"LastName":{"value":"","order":6},\
                "BillAddress":{"value":{"Addr1":{"value":wrap_the_text(data['bill_street'],0),"order":1},\
                                        "Addr2":{"value":wrap_the_text(data['bill_street'],1),"order":2},\
                                        "Addr3":{"value":wrap_the_text(data['bill_street'],2),"order":3},\
                                        "City":{"value":data['bill_city'],"order":6},\
                                        "State":{"value":data['bill_state'],"order":7},\
                                        "PostalCode":{"value":data['bill_code'],"order":8},\
                                        "Country":{"value":data['bill_country'],"order":9}},\
                               "order":7},\
                "ShipAddress":{"value":{"Addr1":{"value":wrap_the_text(data['ship_street'],0),"order":1},\
                                        "Addr2":{"value":wrap_the_text(data['ship_street'],1),"order":2},\
                                        "Addr3":{"value":wrap_the_text(data['ship_street'],2),"order":3},\
                                        "City":{"value":data['ship_city'],"order":6},\
                                        "State":{"value":data['ship_state'],"order":7},\
                                        "PostalCode":{"value":data['ship_code'],"order":8},\
                                        "Country":{"value":data['ship_country'],"order":9}},\
                               "order":8},\
                "Phone":{"value":data.get('phone','0'),"order":9},"AltPhone":{"value":data.get('otherphone','0'),"order":10},\
                "Fax":{"value":data.get('fax','0'),"order":11},"Email":{"value":data['email1'],"order":12},"Cc":{"value":data['email2'],"order":13},\
                "Contact":{"value":data['contact']['firstname'],"order":14},\
                "Notes":{"value":data['record_id'],"order":15}}}
    return tag_dct

def get_potential_new_xml(pot_data, data):
    
    truncated_account_name = get_truncated_name(data['account_no'], data['accountname'])
    truncated_potential_name = get_truncated_name(pot_data['potential_no'], pot_data['potentialname'])
    
    tag_dct = {"roottag":{"tagname":"CustomerAddRq","id":1},"basetag":{"tagname":"CustomerAdd","id":1},
               "tags":{"Name":{"value":truncated_potential_name,"order":1},"IsActive":{"value":"1","order":2},\
                       "ParentRef":{"value":{"ListID":{"value":data['quickbooks_id'],"order":1}},"order":3},\
                       "CompanyName":{"value":data['accountname'],"order":4},"Salutation":{"value":pot_data['contact']['salutationtype'],"order":5},\
                       "FirstName":{"value":pot_data['contact']['firstname'],"order":6},"LastName":{"value":pot_data['contact']['lastname'],"order":7},\
                       "BillAddress":{"value":{"Addr1":{"value":wrap_the_text(data['bill_street'],0),"order":1},\
                                        "Addr2":{"value":wrap_the_text(data['bill_street'],1),"order":2},\
                                        "Addr3":{"value":wrap_the_text(data['bill_street'],2),"order":3},\
                                        "City":{"value":data['bill_city'],"order":6},\
                                        "State":{"value":data['bill_state'],"order":7},\
                                        "PostalCode":{"value":data['bill_code'],"order":8},\
                                        "Country":{"value":data['bill_country'],"order":9}},\
                               "order":8},\
                       "ShipAddress":{"value":{"Addr1":{"value":wrap_the_text(data['ship_street'],0),"order":1},\
                                        "Addr2":{"value":wrap_the_text(data['ship_street'],1),"order":2},\
                                        "Addr3":{"value":wrap_the_text(data['ship_street'],2),"order":3},\
                                        "City":{"value":data['ship_city'],"order":6},\
                                        "State":{"value":data['ship_state'],"order":7},\
                                        "PostalCode":{"value":data['ship_code'],"order":8},\
                                        "Country":{"value":data['ship_country'],"order":9}},\
                               "order":9},\
                       "Phone":{"value":data.get('phone','0'),"order":10},"AltPhone":{"value":data.get('otherphone','0'),"order":11},\
                       "Fax":{"value":data.get('fax','0'),"order":12},"Email":{"value":data['email1'],"order":13},"Cc":{"value":data['email2'],"order":14},\
                       "Contact":{"value":pot_data['contact']['salutationtype']+" "+pot_data['contact']['firstname']+" "+pot_data['contact']['lastname'],"order":15},\
                       "JobStatus":{"value":"InProgress","order":16},\
                       "Notes":{"value":pot_data['record_id'],"order":17}}}
    return tag_dct

def get_potential_update_xml(pot_data, data):

    truncated_account_name = get_truncated_name(data['account_no'], data['accountname'])
    truncated_potential_name = get_truncated_name(pot_data['potential_no'], pot_data['potentialname'])
    edit_sequence_value = get_latest_record_from_qb(pot_data, 'potential', 'edit_sequence_tag')
    
    tag_dct = {"roottag":{"tagname":"CustomerModRq","id":1},"basetag":{"tagname":"CustomerMod","id":1},
               "tags":{"ListID":{"value":pot_data['quickbooks_id'],"order":1},"EditSequence":{"value":edit_sequence_value,"order":2},\
                       "Name":{"value":truncated_potential_name,"order":3},"IsActive":{"value":"1","order":4},\
                       "ParentRef":{"value":{"ListID":{"value":data['quickbooks_id'],"order":1}},"order":5},\
                       "CompanyName":{"value":data['accountname'],"order":6},"Salutation":{"value":pot_data['contact']['salutationtype'],"order":7},\
                       "FirstName":{"value":pot_data['contact']['firstname'],"order":8},"LastName":{"value":pot_data['contact']['lastname'],"order":9},\
                       "BillAddress":{"value":{"Addr1":{"value":wrap_the_text(data['bill_street'],0),"order":1},\
                                        "Addr2":{"value":wrap_the_text(data['bill_street'],1),"order":2},\
                                        "Addr3":{"value":wrap_the_text(data['bill_street'],2),"order":3},\
                                        "City":{"value":data['bill_city'],"order":6},\
                                        "State":{"value":data['bill_state'],"order":7},\
                                        "PostalCode":{"value":data['bill_code'],"order":8},\
                                        "Country":{"value":data['bill_country'],"order":9}},\
                               "order":10},\
                       "ShipAddress":{"value":{"Addr1":{"value":wrap_the_text(data['ship_street'],0),"order":1},\
                                        "Addr2":{"value":wrap_the_text(data['ship_street'],1),"order":2},\
                                        "Addr3":{"value":wrap_the_text(data['ship_street'],2),"order":3},\
                                        "City":{"value":data['ship_city'],"order":6},\
                                        "State":{"value":data['ship_state'],"order":7},\
                                        "PostalCode":{"value":data['ship_code'],"order":8},\
                                        "Country":{"value":data['ship_country'],"order":9}},\
                               "order":11},\
                       "Phone":{"value":data.get('phone','0'),"order":12},"AltPhone":{"value":data.get('otherphone','0'),"order":13},\
                       "Fax":{"value":data.get('fax','0'),"order":14},"Email":{"value":data['email1'],"order":15},"Cc":{"value":data['email2'],"order":16},\
                       "Contact":{"value":pot_data['contact']['salutationtype']+" "+pot_data['contact']['firstname']+" "+pot_data['contact']['lastname'],"order":17},\
                       "JobStatus":{"value":"InProgress","order":18},\
                       "Notes":{"value":pot_data['record_id'],"order":19}}}
    return tag_dct

def get_account_update_xml(data, relative_data=None):

    truncated_account_name = get_truncated_name(data['account_no'], data['accountname'])
    data['account_qbid'] = data['quickbooks_id']
    edit_sequence_value = get_latest_record_from_qb(data, 'account', 'edit_sequence_tag')
    
    tag_dct = {"roottag":{"tagname":"CustomerModRq","id":1},"basetag":{"tagname":"CustomerMod","id":1},
               "tags":{"ListID":{"value":data['quickbooks_id'],"order":1},"EditSequence":{"value":edit_sequence_value,"order":2},\
                       "Name":{"value":data['accountname'],"order":3},"IsActive":{"value":"1","order":4},\
                       "CompanyName":{"value":data['accountname'],"order":5},"Salutation":{"value":"","order":6},\
                       "FirstName":{"value":"","order":7},"LastName":{"value":"","order":8},\
                       "BillAddress":{"value":{"Addr1":{"value":wrap_the_text(data['bill_street'],0),"order":1},\
                                        "Addr2":{"value":wrap_the_text(data['bill_street'],1),"order":2},\
                                        "Addr3":{"value":wrap_the_text(data['bill_street'],2),"order":3},\
                                        "City":{"value":data['bill_city'],"order":6},\
                                        "State":{"value":data['bill_state'],"order":7},\
                                        "PostalCode":{"value":data['bill_code'],"order":8},\
                                        "Country":{"value":data['bill_country'],"order":9}},\
                               "order":9},\
                       "ShipAddress":{"value":{"Addr1":{"value":wrap_the_text(data['ship_street'],0),"order":1},\
                                        "Addr2":{"value":wrap_the_text(data['ship_street'],1),"order":2},\
                                        "Addr3":{"value":wrap_the_text(data['ship_street'],2),"order":3},\
                                        "City":{"value":data['ship_city'],"order":6},\
                                        "State":{"value":data['ship_state'],"order":7},\
                                        "PostalCode":{"value":data['ship_code'],"order":8},\
                                        "Country":{"value":data['ship_country'],"order":9}},\
                               "order":10},\
                       "Phone":{"value":data.get('phone','0'),"order":11},"AltPhone":{"value":data.get('otherphone','0'),"order":12},\
                       "Fax":{"value":data.get('fax','0'),"order":13},"Email":{"value":data['email1'],"order":14},"Cc":{"value":data['email2'],"order":15},\
                       #"Contact":{"value":data['contact']['firstname'],"order":16},\
                       "Notes":{"value":data.get('record_id',data['id']),"order":17}}}
    return tag_dct

def get_latest_record_from_qb(data, entity_type, tag_to_parse):
    
    fun_name = 'get_'+entity_type+'_query_xml'
    base_tag = qb_module_mapping[entity_type]['base_tag']
    xml_req = eval(fun_name)(data)
    qbxml_query = generate_qbxml_query(xml_req)
    response_string = quick_book_connector(qbxml_query)
    return parse_response(entity_type, response_string, base_tag, qb_module_mapping[entity_type][tag_to_parse])

def get_contact_new_xml(data, relative_data=None):
    pass

def get_product_new_xml(data, relative_data=None):
    xml = """
            <ItemServiceAddRq>
                    <ItemServiceAdd>
                        <Name>%s</Name>
                        <SalesOrPurchase>
                            <Desc>%s</Desc>
                            <Price>%s</Price>
                            <AccountRef>
                                <FullName>Sales:%s</FullName>
                            </AccountRef>
                        </SalesOrPurchase>
                    </ItemServiceAdd>
                </ItemServiceAddRq>
            """ % (data['productcode'], data['id']+':::'+data['productname'], str(float(data['unit_price'])), data['productcategory'])
    return xml

def get_product_update_xml(data, relative_data=None):
    edit_sequence_value = get_latest_record_from_qb(data, 'product', 'edit_sequence_tag')
    xml = """
            <ItemServiceModRq>
                    <ItemServiceMod>
                        <ListID>%s</ListID> <!-- required -->
                        <EditSequence>%s</EditSequence> <!-- required -->
                        <Name>%s</Name>
                        <SalesOrPurchaseMod>
                            <Desc>%s</Desc>
                            <Price>%s</Price>
                            <AccountRef>
                                <FullName>Income</FullName>
                            </AccountRef>
                        </SalesOrPurchaseMod>
                    </ItemServiceMod>
                </ItemServiceModRq>
            """ % (data['quickbooks_id'], edit_sequence_value, data['productcode'], data['id']+':::'+data['productname'], str(float(data['unit_price'])))
    return xml

def get_salesorder_new_xml(data, relative_data=None):
    txn_date = datetime.strftime(datetime.strptime(data['createdtime'],"%Y-%m-%d %H:%M:%S"), "%Y-%m-%d")
    xml = """
        <SalesOrderAddRq requestID="6">
            <SalesOrderAdd defMacro="TxnID:Sale%s">
                <CustomerRef>
                    <ListID>%s</ListID>
                </CustomerRef>
                <TxnDate>%s</TxnDate>
                <RefNumber>%s</RefNumber>"""  % (data['salesorder_no'], data['potential_qbid'], txn_date, data['record_id'])
                
    for line_item in data['LineItems']:
        xml += """<SalesOrderLineAdd>
            <ItemRef>
                <ListID>%s</ListID>
            </ItemRef>
            <Quantity>%s</Quantity>
            <Rate>%s</Rate>
            <Other1>%s</Other1>
            <Other2>%s</Other2>
        </SalesOrderLineAdd>""" % (line_item['product_qbid'], str(int(float(line_item['quantity']))), str(float(line_item['listprice'])), line_item['lineitem_id'], line_item['sequence_no'])
        
    xml += """</SalesOrderAdd>
        </SalesOrderAddRq>      
    """
    return xml

def get_salesorder_update_xml(data, relative_data=None):
    data['salesorder_qbid'] = data['quickbooks_id']
    edit_sequence_value = get_latest_record_from_qb(data, 'salesorder', 'edit_sequence_tag')
    txnline_id_value = get_latest_record_from_qb(data, 'salesorder', 'txn_line_id_tag')
    line_dct = {}
    for txnline in txnline_id_value:
        line_dct[txnline['crm_sequence_no']] = txnline
    txn_date = datetime.strftime(datetime.strptime(data['createdtime'],"%Y-%m-%d %H:%M:%S"), "%Y-%m-%d")
    xml = """
        <SalesOrderModRq>
            <SalesOrderMod>
                <TxnID>%s</TxnID>
                <EditSequence>%s</EditSequence>
                <CustomerRef>
                    <ListID>%s</ListID>
                </CustomerRef>
                <TxnDate>%s</TxnDate>
                <RefNumber>%s</RefNumber>""" % (data['quickbooks_id'], edit_sequence_value, data['potential_qbid'], txn_date, data['record_id'])
    
    for line_item in data['LineItems']:
        try:
            txn = line_dct[line_item['sequence_no']]['qb_txn_line_id']
        except:
            txn = '-1'
    
        xml += """<SalesOrderLineMod>
                    <TxnLineID>%s</TxnLineID>
                    <ItemRef>
                        <ListID>%s</ListID>
                    </ItemRef>
                    <Quantity>%s</Quantity>
                    <Rate>%s</Rate>
                    <Other1>%s</Other1>
                    <Other2>%s</Other2>
                </SalesOrderLineMod>""" % (txn, line_item['product_qbid'], str(float(line_item['quantity'])), str(float(line_item['listprice'])), line_item['lineitem_id'], line_item['sequence_no'])
    xml += """</SalesOrderMod>
        </SalesOrderModRq>"""
    return xml

'''
def get_invoice_new_xml(data, relative_data=None):
    txn_date = datetime.strftime(datetime.strptime(data['createdtime'],"%Y-%m-%d %H:%M:%S"), "%Y-%m-%d")
    xml = """
        <InvoiceAddRq requestID="5">
            <InvoiceAdd defMacro="TxnID:%s">
              <CustomerRef>
                <ListID>%s</ListID>
              </CustomerRef>
              <TxnDate>%s</TxnDate>
              <RefNumber>%s</RefNumber>""" % (data['invoice_no'], data['potential_qbid'], txn_date, data['record_id'])

    for line_item in data['LineItems']:
        xml += """
              <InvoiceLineAdd>
                <ItemRef>
                  <ListID>%s</ListID> <!-- optional -->
                </ItemRef>
                <Quantity>%s</Quantity>
                <Rate>%s</Rate>
                <Other1>%s</Other1>
                <Other2>%s</Other2>
              </InvoiceLineAdd>""" % (line_item['product_qbid'], line_item['quantity'], str(float(line_item['listprice'])), line_item['lineitem_id'], line_item['sequence_no'])
    xml += """
            </InvoiceAdd>
          </InvoiceAddRq>"""
    
    return xml
'''

def get_invoice_new_xml(data, relative_data=None):
    return generate_credit_invoice_xml(data, 'Invoice', relative_data=None)

def get_creditmemo_new_xml(data, relative_data=None):
    return generate_credit_invoice_xml(data, 'CreditMemo', relative_data=None)
      
def generate_credit_invoice_xml(data, entity_name, relative_data=None):
    
    xml = """
          <%sAddRq requestID="5">
           <%sAdd defMacro="TxnID:%s"> <!-- required -->
            <CustomerRef> <!-- required -->
             <ListID>%s</ListID> <!-- optional -->
            </CustomerRef>
            <TxnDate>%s</TxnDate> <!-- optional -->
            <RefNumber>%s</RefNumber> <!-- optional -->
            <BillAddress> <!-- optional -->
             <Addr1>%s</Addr1> <!-- optional -->
             <City>%s</City> <!-- optional -->
             <State>%s</State> <!-- optional -->
             <PostalCode>%s</PostalCode> <!-- optional -->
             <Country>%s</Country> <!-- optional -->
            </BillAddress>
            <ShipAddress> <!-- optional -->
             <Addr1>%s</Addr1> <!-- optional -->
             <City>%s</City> <!-- optional -->
             <State>%s</State> <!-- optional -->
             <PostalCode>%s</PostalCode> <!-- optional -->
             <Country>%s</Country> <!-- optional -->
            </ShipAddress>
            <DueDate>%s</DueDate> <!-- optional -->
            <Other>%s</Other><!-- optional -->""" % (entity_name, entity_name, data['invoice_no'], data['potential_qbid'], data['invoicedate'], data[inv_ref_no_key], data['bill_street'], data['bill_city'], data['bill_state'], data['bill_pobox'], data['bill_country'], data['ship_street'], data['ship_city'], data['ship_state'], data['ship_pobox'], data['ship_country'], data['duedate'], remove_tabid(data['record_id']))

    for line_item in data['LineItems']:

         xml += """
            <%sLineAdd> <!-- optional -->
             <ItemRef> <!-- optional -->
              <ListID>%s</ListID> <!-- optional -->
             </ItemRef>
             <Desc>%s :: %s</Desc> <!-- optional -->
             <Quantity>%s</Quantity> <!-- optional -->
             <Rate>%s</Rate> <!-- optional -->
             <Other1>%s</Other1> <!-- optional -->
             <Other2>%s</Other2> <!-- optional -->
            </%sLineAdd>""" % (entity_name, line_item['product_entity'][prod_qb_id_key], line_item['product_entity']['productname'], line_item['comment'], line_item['quantity'], str(float(line_item['listprice'])), line_item['lineitem_id'], line_item['sequence_no'], entity_name)
        
    xml += """</%sAdd>
           </%sAddRq>""" % (entity_name, entity_name)

    return xml

def get_account_query_xml(data, relative_data=None):
    return generate_query_xml(data['account_qbid'],'account')

def get_salesorder_query_xml(data, relative_data=None):
    return generate_query_xml(data['salesorder_qbid'], 'salesorder')

def get_potential_query_xml(data, relative_data=None):
    return generate_query_xml(data['potential_qbid'], 'potential')

def get_product_query_xml(data, relative_data=None):
    return generate_query_xml(data['product_qbid'],'product')

def generate_query_xml(list_id_val, entity_type):
    query_req_tag = qb_module_mapping[entity_type]['query_req_tag']
    list_id_tag = qb_module_mapping[entity_type]['list_id_tag']
    inc_line_itms = ''
    if(entity_type == 'salesorder' or entity_type == 'invoice'):
        inc_line_itms = '<IncludeLineItems>1</IncludeLineItems>'
    xml = """<%s>
                <%s>%s</%s>
                %s
             </%s>""" % (query_req_tag, list_id_tag, list_id_val, list_id_tag, inc_line_itms, query_req_tag)
    return xml
    

