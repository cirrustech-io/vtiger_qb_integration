#!/usr/bin/env python
""" 
==============================================================================
title           : rmqTask.py
description     : This file is used to publish and consume messages from rabbitmq
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python rmqTask.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import pika
from rmqConnection import connection
from rmqConfig import MAIN_EXCHANGE, QUEUE_MAP 
import socket
from execute_tasks import *

host_name = socket.getfqdn()

class Task:

    def __init__(self):
        self.connection = ''
        self.channel = ''
        self.queue = ''
        self.message = ''
        self.main_exchange = MAIN_EXCHANGE
        self.queue_map = QUEUE_MAP
        
    def open_connection(self):
        connect = connection().get_connection()
        chan = connect.channel()
        return connect, chan

    def get_logging(self, message):
        
        self.connection, self.channel = self.open_connection()
        self.queue = 'logstash'
        self.channel.queue_declare(queue = self.queue, durable = True)
        self.message = message #' '.join(sys.argv[1:]) or "Hello World!"
        message_prop = pika.BasicProperties(
                         content_type = 'application/json',
                         delivery_mode = 2, # make message persistent
                      )
        self.channel.basic_publish(exchange = '',
                      routing_key = self.queue,
                      body = self.message,
                      properties = message_prop)
        #print " [x] Sent %r" % (self.message)
    
    def close_connection(self):
        self.connection.close()

    def publish_message(self, message, code):
        self.connection, self.channel = self.open_connection()
        self.message = message
        self.queue = self.queue_map[code]['queue_name']
        self.routing_key = self.queue_map[code]['routing_key']
        message_prop = pika.BasicProperties(
                         content_type = 'application/json',
                         delivery_mode = 2, # make message persistent
                      )
        self.channel.basic_publish(exchange = self.main_exchange['name'],
                      routing_key = self.routing_key,
                      body = self.message,
                      properties = message_prop)
        print " Published message - %s to %s" % (self.message, code)

    def consume_message(self, dest_exchange, queue_name, routing_key):

        self.connection, self.channel = self.open_connection()
        result = self.channel.queue_declare(queue=queue_name, durable=True)
        queue_name = result.method.queue

        self.channel.queue_bind(exchange=dest_exchange, queue=queue_name, routing_key=routing_key)
        print ' [*] Waiting for message. To exit press CTRL+C'

        def callback(ch, method, properties, body):
            print " [x] %r:%r" % (method.routing_key, body,)
            resp = eval(queue_name)(json.loads(body), method.routing_key)
            print "%s - %s" % (queue_name,resp)
         
        self.channel.basic_consume(callback, queue=queue_name, no_ack=True)
        self.channel.start_consuming()
