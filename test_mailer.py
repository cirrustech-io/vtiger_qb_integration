#!/usr/bin/env python
""" 
==============================================================================
title           : test_mailer.py
description     : This file is used to test smtp mailer
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python test_mailer.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

# Import smtplib for the actual sending function
import smtplib

# For guessing MIME type
import mimetypes

# Import the email modules we'll need
import email
import email.mime.application

# Create a text/plain message
msg = email.mime.Multipart.MIMEMultipart()
msg['Subject'] = 'Invoice'
msg['From'] = 'crm@i.infonas.com'
msg['To'] = 'binny.abraham@infonas.com'

# The main body is just another attachment
body = email.mime.Text.MIMEText("""Testing the python script as a service. """)
msg.attach(body)

# PDF attachment
# filename='./invoice/INV17_ACME_Corp.pdf'
# fp=open(filename,'rb')
# att = email.mime.application.MIMEApplication(fp.read(),_subtype="pdf")
# fp.close()
# att.add_header('Content-Disposition','attachment',filename=filename)
# msg.attach(att)

# send via Gmail server
# NOTE: my ISP, Centurylink, seems to be automatically rewriting
# port 25 packets to be port 587 and it is trashing port 587 packets.
# So, I use the default port 25, but I authenticate. 
s = smtplib.SMTP("mail.i.infonas.com",25)
s.sendmail('crm@i.infonas.com',['binny.abraham@infonas.com'], msg.as_string())
s.quit()
