#!/usr/bin/env python
"""
==============================================================================
title           : invoice_generator.py
description     : This is PDF invoice generator file
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python invoice_generator.py
notes           :
python_version  : 2.7.9
==============================================================================
"""

import pdfkit
from invoice_mailer import attachment_mailer, generate_invoice_file_path
from datetime import datetime
from qbcrm_config import site_url
from qbcrm_helper import inv_ref_no_key, circuit_id_key, remove_tabid
from vtiger_connect import get_retrieve

invoice_path = './invoice/'

def generate_invoice(data, routing_key, invoice_flag=False):

    invoice_html, product_name, invoice_num, reciever_mail = generate_invoice_html(data, invoice_flag)
    invoice_pdf = generate_invoice_pdf(invoice_html, product_name, invoice_num)
    attachment_mailer(product_name, invoice_num, reciever_mail)
    return invoice_pdf

def generate_invoice_html(data, invoice_flag):

    print data
    account_data = data['payload']['account']
    invoice_data = data['payload']['invoice']
    account_name = account_data['accountname']
    reciever_mail = ''#account_data['email1']
    invoice_current = 0.0
    invoice_balance = 0.0
    invoice_total = 0.0

    invoice_num = invoice_data[0][inv_ref_no_key]
    address_street = invoice_data[0]['bill_street']
    address_city = invoice_data[0]['bill_city']+""" """+invoice_data[0]['bill_country']
    invoice_date = invoice_data[0]['invoicedate']
    invoice_due = invoice_data[0]['duedate']

    invoice_html = """<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
        <title>Invoice</title>
        <link rel='stylesheet' type='text/css' href='"""+site_url+"""/test/invoice/EditableInvoice/css/style.css' />
        <link rel='stylesheet' type='text/css' href='"""+site_url+"""/test/invoice/EditableInvoice/css/print.css' media="print" />
        <script type='text/javascript' src='"""+site_url+"""/test/invoice/EditableInvoice/js/jquery-1.3.2.min.js'></script>
        <script type='text/javascript' src='"""+site_url+"""/test/invoice/EditableInvoice/js/example.js'></script>
        <style>
          body {
            background-image:url('"""+site_url+"""/test/logo/Infonas_Letterhead_Small.jpg');
            background-repeat: repeat-x;
           }
        </style>
    </head>

    <body style="width:795px; height:1123px">
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <div style="clear:both"></div>
        <div id="page-wrap">

            <div style="clear:both"></div>
            <div id="customer">
                <span style="float:left; border:1px solid; padding-left:5px;">
                        <b>Bill To</b>
                        <br />
                         """+account_name+"""
                        <br />
                         """+address_street+"""
                        <br />
                         """+address_city+"""
                </span>
                <span style="float:right">
                    <table id="meta">
                        <tr>
                            <td class="meta-head">Invoice #</td>
                            <td><textarea>INV:"""+invoice_num+"""</textarea></td>
                        </tr>
                        <tr>
                            <td class="meta-head">Invoice Date</td>
                            <td><textarea id="due">"""+invoice_date+"""</textarea></td>
                        </tr>
                        <tr>
                            <td class="meta-head">Due Date</td>
                            <td><div class="due">"""+invoice_due+"""</div></td>
                        </tr>
                    </table>
                </span>
            </div>

            <table id="items">
                  <tr>
                      <th>No</th>
                      <th>Description</th>
                      <th>CircuitID</th>
                      <th>Qty</th>
                      <th>Cost</th>
                      <th>Price</th>
                  </tr>""";

    sq_no = 1
    for inv_dat in invoice_data:
        if inv_dat:
            sale_data = get_retrieve(inv_dat['salesorder_id'])
            pot_data = get_retrieve(sale_data['potential_id'])
            for item in inv_dat['LineItems']:
                if item:
                    product_data = get_retrieve(item['productid'])
                    amount = float(item['quantity'])*float(item['listprice'])
                    invoice_html += """
                    <tr class="item-row">
                      <td>
                          """+str(sq_no)+""".
                      </td>
                      <td>
                        """+product_data['productname']+""" -- """+item['comment']+"""
                      </td>
                      <td>
                          """+pot_data[circuit_id_key]+"""
                      </td>
                      <td>
                        """+str(int(float(item['quantity'])))+"""
                      </td>
                      <td>
                        """+str(float(item['listprice']))+"""
                      </td>
                      <td>
                        """+str(amount)+"""
                      </td>
                    </tr>"""

                    sq_no +=1
            invoice_current += float(inv_dat['hdnSubTotal'])
            invoice_balance += float(inv_dat['txtAdjustment'])
            invoice_total += float(inv_dat['hdnGrandTotal'])

    invoice_html += """
                  <tr>
                      <td colspan="2" class="blank"> </td>
                      <td colspan="3" class="total-line">Current Charges</td>
                      <td class="total-value"><div id="subtotal">"""+str(invoice_current)+"""</div></td>
                  </tr>
                  <tr>
                      <td colspan="2" class="blank"> </td>
                      <td colspan="3" class="total-line">Outstanding Balance</td>
                      <td class="total-value"><div id="total">"""+str(invoice_balance)+"""</div></td>
                  </tr>
                  <tr>
                      <td colspan="2" class="blank"> </td>
                      <td colspan="3" class="total-line balance">Balance Due</td>
                      <td class="total-value balance"><div class="due">"""+str(invoice_total)+"""</div></td>
                  </tr>

            </table>

            <div id="terms">
                <span style="padding-left:5px;">
                  Payment Mode: Cash/CHQ/Credit Card
                  <br />
                  Pay by cash/chq at: NBB Tower, 12th Flr, Manama
                </span>
            </div>
        </div>
    </body>
</html>"""

    return invoice_html, account_name, invoice_num, reciever_mail

def generate_invoice_pdfcrowd(invoice_html):
    import pdfcrowd
    client = pdfcrowd.Client("binnyabraham", "5a766cbae152f3ec5783eeb7d5807347")
    output_file = open('invoice_out.pdf', 'wb')
    client.convertHtml(invoice_html, output_file)
    output_file.close()
    return invoice_html

def generate_invoice_pdf(invoice_html, product_name, invoice_num):
    invoice_file = generate_invoice_file_path(product_name, invoice_num, '.pdf')
    pdfkit.from_string(invoice_html, invoice_file)
