#!/usr/bin/env python
""" 
==============================================================================
title           : qbsdktest.py
description     : This file is to test the quick books integration 
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python qbsdktest.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import win32com.client
import xml.etree.ElementTree
import MySQLdb
from yattag import Doc
import time
from datetime import datetime
import csv
import random
from qbcrm_config import qb_module_mapping, qb_company_file, qb_user_mode
import os

crm_host = '80.88.242.28'
crm_user = 'treeio'
crm_passwd = 'treeio'
crm_db = 'treeio'

import logging
# create logger
lgr = logging.getLogger('qbsdk')
lgr.setLevel(logging.INFO)
# add a file handler
file_path = os.path.join("D:\\", "Integration", "vtiger_qb_integration", "qbsdk.log")
fh = logging.FileHandler(file_path)
fh.setLevel(logging.INFO)
# create a formatter and set the formatter for the handler.
frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(frmt)
# add the Handler to the logger
lgr.addHandler(fh)

"""
# You can now start issuing logging statements in your code
lgr.debug('debug message') # This won't print to myapp.log
lgr.info('info message') # Neither will this.
lgr.warn('Checkout this warning.') # This will show up in the log file.
lgr.error('An error goes here.') # and so will this.
lgr.critical('Something critical happened.') # and this one too.
"""

def get_data_from_crm(sql_query):
    try:
            crm_dbh = MySQLdb.connect( host=crm_host, user=crm_user, passwd=crm_passwd, db=crm_db )
            crm_cursor = crm_dbh.cursor()

            # Get Accounts from CRM
            crm_cursor.execute(sql_query)
            data = crm_cursor.fetchall()
            crm_dbh.commit()
            crm_cursor.close()
            crm_dbh.close()
            return data
    except MySQLdb.Error, e:
            print "MYSQL:",e
            return "MySQL Error %d: %s" % (e.args[0], e.args[1])

def generate_qbxml_query(xml_req):
    # Generate qb xml request
      
    qbxml_query = """
        <?qbxml version="13.0"?>
        <QBXML><!-- onError may be set to continueOnError or stopOnError. -->
            <QBXMLMsgsRq onError="stopOnError"><!-- Begin first request: adding a customer -->
                %s
            </QBXMLMsgsRq>
        </QBXML>
    """ % (xml_req)
    return qbxml_query

def parse_response(response_string, base_tag, full_name_tag, list_id_tag):

    # Parse the response into an Element Tree and peel away the layers of response
    
    QBXML = xml.etree.ElementTree.fromstring(response_string)
    QBXMLMsgsRs = QBXML.find('QBXMLMsgsRs')
    QueryResponse = QBXMLMsgsRs.getiterator(base_tag)

    fullname = ''
    listid = ''
    if QueryResponse:
        QueryRet = QueryResponse[0]
        try:
            fullname = QueryRet.find(full_name_tag).text
        except:
            fullname = ''
        try:
            listid = QueryRet.find(list_id_tag).text
        except:
            listid = ''
    else:
        print response_string
    return fullname, listid

    """
    # Parse the response into an Element Tree and peel away the layers of response
    QBXML = xml.etree.ElementTree.fromstring(response_string)
    QBXMLMsgsRs = QBXML.find('QBXMLMsgsRs')
    InventoryAdjustmentQueryRs = QBXMLMsgsRs.getiterator("InventoryAdjustmentRet")
    for InvAdjRet in InventoryAdjustmentQueryRs:
        txnid = InvAdjRet.find('TxnID').text
        memo = InvAdjRet.find('memo').text
        print txnid
        print memo
    """

def quick_book_connector(qbxml_query):
    # Connect to Quickbooks
    
    sessionManager = win32com.client.Dispatch("QBXMLRP2.RequestProcessor")
    sessionManager.OpenConnection('', qb_company_file)
    ticket = sessionManager.BeginSession("", qb_user_mode['multi_user'])

    # Send query and receive response
    response_string = sessionManager.ProcessRequest(ticket, qbxml_query)

    # Disconnect from Quickbooks
    sessionManager.EndSession(ticket)     # Close the company file
    sessionManager.CloseConnection()      # Close the connection
    return response_string

def refine_data(data):
    if data:
        return str(data)
    else:
        return ""

def generate_req_xml(tag_dct):

    import pdb
    pdb.set_trace()
    doc, tag, text = Doc().tagtext()
    with tag(tag_dct['roottag']['tagname'], requestID=str(tag_dct['roottag']['id'])):
        with tag(tag_dct['basetag']['tagname']):
            for val in sorted(tag_dct['tags'].items(), key=lambda x: x[1]):
                with tag(val[0]):
                    if type(val[1]['value']) == dict:
                        for ite in sorted(val[1]['value'].items(), key=lambda y: y[1]):
                            with tag(ite[0]):
                                text(str(ite[1]['value']))
                    else:
                        text(str(val[1]['value']))
    return doc.getvalue()

def generate_customer_add_req(data, contact_data):

    doc, tag, text = Doc().tagtext()
    with tag('CustomerAddRq', requestID='1'):
        with tag('CustomerAdd'):
            with tag('Name'):
                text(refine_data(data[1][:40]))
            with tag('IsActive'):
                text('1')
            with tag('CompanyName'):
                text(refine_data(data[1][:40]))
            with tag('BillAddress'):
                with tag('Addr1'):
                    text(refine_data(data[12]))
                with tag('City'):
                    text(refine_data(data[8]))
                with tag('State'):
                    text(refine_data(data[10]))
                with tag('PostalCode'):
                    text(refine_data(data[13]))
                with tag('Country'):
                    text(refine_data(data[11]))
                with tag('Note'):
                    text(refine_data(data[9]))
            with tag('ShipAddress'):
                with tag('Addr1'):
                    text(refine_data(data[18]))
                with tag('City'):
                    text(refine_data(data[14]))
                with tag('State'):
                    text(refine_data(data[16]))
                with tag('PostalCode'):
                    text(refine_data(data[19]))
                with tag('Country'):
                    text(refine_data(data[17]))
                with tag('Note'):
                    text(refine_data(data[15]))
            with tag('Phone'):
                text(refine_data(data[2]))
            with tag('AltPhone'):
                text(refine_data(data[3]))
            with tag('Fax'):
                text(refine_data(data[4]))
            with tag('Email'):
                text(refine_data(data[5]))
            with tag('Cc'):
                text(refine_data(data[6]))
            if len(contact_data) > 0:
                with tag('Contact'):
                    text(refine_data(contact_data[0]))
            if len(contact_data) > 1:
                with tag('AltContact'):
                    text(refine_data(contact_data[1]))
            with tag('Notes'):
                text("CRM Id : "+str(refine_data(data[0])))
                
    return doc.getvalue()

def generate_service_add_req(data):

    xml = """
            <ItemServiceAddRq>
                    <ItemServiceAdd>
                        <Name>%s</Name>
                        <SalesOrPurchase>
                            <Desc>%s Id : %s</Desc>
                            <Price>%s</Price>
                            <AccountRef>
                                <FullName>Sales - Hardware</FullName>
                            </AccountRef>
                        </SalesOrPurchase>
                    </ItemServiceAdd>
                </ItemServiceAddRq>Name
            """ % (data[1], data[2], str(data[0]), data[3])
    
    return xml

def generate_saleorder_add_req(data, qbr_data=None):

    txn_date = datetime.strftime(datetime.now(),"%Y-%m-%d")

    xml = """
        <SalesOrderAddRq requestID="6">
            <SalesOrderAdd defMacro="TxnID:Sale%s"> <!-- required -->
                <CustomerRef> <!-- required -->
                    <ListID>%s</ListID> <!-- optional -->
                </CustomerRef>
                <TxnDate>%s</TxnDate> <!-- optional -->
                <RefNumber>%s</RefNumber> <!-- optional -->
                <Other>Sale Order Id : %s</Other> <!-- optional -->
                <SalesOrderLineAdd> <!-- optional -->
                    <ItemRef> <!-- optional -->
                        <ListID>80000012-1409147100</ListID> <!-- optional -->
                        <FullName>Internet</FullName> <!-- optional -->
                    </ItemRef>
                    <Desc>Product Id - 32309</Desc> <!-- optional -->
                    <Quantity>1</Quantity> <!-- optional -->
                    <Rate>20.00</Rate> <!-- optional -->
                </SalesOrderLineAdd>
            </SalesOrderAdd>
        </SalesOrderAddRq>      
    """ % (str(data[0]), data[1], txn_date, data[2], str(data[0])) #% (str(data[0]), qbr_data[0], txn_date, data[1], str(data[0]))
    return xml

def generate_invoice_add_req(data, qbr_data=None, qbr_prd_data=None):

    txn_date = datetime.strftime(datetime.now(),"%Y-%m-%d")
    
    xml = """
        <InvoiceAddRq requestID="5">
            <InvoiceAdd defMacro="TxnID:Inv%s">
              <CustomerRef>
                <ListID>%s</ListID>
              </CustomerRef>
              <TxnDate>%s</TxnDate>
              <RefNumber>INV:%s</RefNumber>
              <Other>CRM Invoice Id : %s</Other>
              <InvoiceLineAdd>
                <ItemRef>
                  <ListID>%s</ListID> <!-- optional -->
                  <FullName>%s</FullName>
                </ItemRef>
                <Desc>CRM Product Id - %s</Desc>
                <Quantity>%s</Quantity>
                <Rate>%s</Rate>
              </InvoiceLineAdd>
            </InvoiceAdd>
          </InvoiceAddRq>
    """ % (str(data[0]), data[1], txn_date, data[0], data[0], '80000012-1409147100', 'Internet', '32309', '1', '20.0') #(str(data[0]), qbr_data[0], txn_date, data[0], data[0], qbr_prd_data[0], qbr_prd_data[1], data[1], data[2], data[3])
    return xml
    

def quick_book(csv_writer, crm_data, entity_type, base_tag):

    counter = 1
    lgr.info(" ::: INSERTION STARTS ::: ")
    full_name_tag = 'FullName'
    list_id_tag = 'ListID'
    for data in crm_data:
        full_name = ''
        list_id = ''
        time_stamp = time.time()
        start_time = datetime.now()
        reference_id = data[0]
        if entity_type == "CustomerAccount":
            contact_query = "select name,lastname from identities_contact where customer_contact_id=%s limit 2;" % (reference_id)
            contact_data = get_data_from_crm(contact_query)
            contact_list = []
            for con in contact_data:
                contact_list.append(con[0])
            xml_req = generate_customer_add_req(data, contact_list)
        elif entity_type == "Service":
            xml_req = generate_service_add_req(data)
        elif entity_type == "SaleOrder":
            customer_id = data[3]
            qbr_query = "select listid,fullname from core_quickboxreference where entitytype='CustomerAccount' and referenceid=%s;" % (customer_id)
            qbr_data = get_data_from_crm(qbr_query)
            xml_req = generate_saleorder_add_req(data, qbr_data[0])
            full_name_tag = 'RefNumber'
            list_id_tag = 'TxnID'
        elif entity_type == "Invoice":
            customer_id = data[5]
            qbr_query = "select listid,fullname from core_quickboxreference where entitytype='CustomerAccount' and referenceid=%s;" % (customer_id)
            qbr_data = get_data_from_crm(qbr_query)
            product_id = data[1]
            qbr_prd_query = "select listid,fullname from core_quickboxreference where entitytype='Service' and referenceid=%s;" % (product_id)
            qbr_prd_data = get_data_from_crm(qbr_prd_query)
            xml_req = generate_invoice_add_req(data, qbr_data[0], qbr_prd_data[0])
            full_name_tag = 'RefNumber'
            list_id_tag = 'TxnID'
        qbxml_query = generate_qbxml_query(xml_req)
        lgr.info("REQUEST %s : %s " % (counter, qbxml_query))
        try:
            response_string = quick_book_connector(qbxml_query)
            full_name, list_id = parse_response(response_string, base_tag, full_name_tag, list_id_tag)
            print list_id
            end_time = datetime.now()
            diff = end_time - start_time
            duration = int(diff.total_seconds() * 1000)
            start = datetime.strftime(start_time,"%Y-%m-%d %H:%M:%S")
            end = datetime.strftime(end_time,"%Y-%m-%d %H:%M:%S")
            insert_query = 'insert into core_quickboxreference (fullname, listid, timestamp, entitytype, referenceid, sequence, duration, starttime, endtime) values ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s");' %(full_name, str(list_id),
                            str(time_stamp), entity_type, reference_id, str(counter), str(duration), start, end)
            get_data_from_crm(insert_query)
            if (list_id):
                write_to_csv([counter, list_id, entity_type, str(duration)], csv_writer)
            lgr.info("RESPONSE %s : %s " % (counter, response_string))
        except Exception,e:
            print e
            lgr.error(str(e))
        
        counter = counter + 1
    lgr.info(" ::: INSERTION ENDS ::: ")

def quick_book_insertion(lgr, xml_req, counter, base_tag, full_name_tag, list_id_tag, entity_type, reference_id, csv_writer=None):

    qbxml_query = generate_qbxml_query(xml_req)
    lgr.info("REQUEST %s : %s " % (counter, qbxml_query))
    time_stamp = time.time()
    start_time = datetime.now()
    try:
        response_string = quick_book_connector(qbxml_query)
        print "RESPONSE : ", response_string
        full_name, list_id = parse_response(response_string, base_tag, full_name_tag, list_id_tag)
        print "QUICKBOOKS ID : ", list_id
#         end_time = datetime.now()
#         diff = end_time - start_time
#         duration = int(diff.total_seconds() * 1000)
#         start = datetime.strftime(start_time,"%Y-%m-%d %H:%M:%S")
#         end = datetime.strftime(end_time,"%Y-%m-%d %H:%M:%S")
#         insert_query = 'insert into core_quickboxreference (fullname, listid, timestamp, entitytype, referenceid, sequence, duration, starttime, endtime) values ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s");' %(full_name, str(list_id),
#                         str(time_stamp), entity_type, reference_id, str(counter), str(duration), start, end)
#         get_data_from_crm(insert_query)
#         if (list_id):
#             write_to_csv([counter, list_id, entity_type, str(duration)], csv_writer)
        lgr.info("RESPONSE %s : %s " % (counter, response_string))
    except Exception,e:
        print e
        lgr.error(str(e))

def quick_book_random(req_type):

    lgr.info(" ::: INSERTION STARTS ::: ")
    full_name_tag = 'FullName'
    list_id_tag = 'ListID'
    base_tag = req_type + 'Ret'
#     filename = 'C:\Users\Binny\Documents\scripts\QuickBook'+req_type+'Random.csv'
#     csv_writer = get_csv_writer(filename)
    entity_type = req_type
    if req_type == 'Customer' or 'ItemService':
        req_range = 2
    else:
        req_range = 2
    for counter in range(1,req_range):
        if req_type == 'Customer':
            tag_dct = {"roottag":{"tagname":"CustomerAddRq","id":1},"basetag":{"tagname":"CustomerAdd","id":1},
                   "tags":{"Name":{"value":get_random_name() + " Customer","order":1},"IsActive":{"value":"1","order":2},\
                    "CompanyName":{"value":get_random_name() + " Company","order":3},"Salutation":{"value":"Mr","order":4},\
                    "FirstName":{"value":get_random_name(),"order":5},"LastName":{"value":get_random_name(),"order":6},\
                    "BillAddress":{"value":{"Addr1":{"value":"Flat No:14","order":1},"City":{"value":"bangalore","order":2},\
                                   "State":{"value":"Karnataka","order":3},"PostalCode":{"value":"123456","order":4},\
                                   "Country":{"value":"India","order":5}},"order":7},\
                    "ShipAddress":{"value":{"Addr1":{"value":"Flat No:14","order":1},"City":{"value":"bangalore","order":2},\
                                   "State":{"value":"Karnataka","order":3},"PostalCode":{"value":"123456","order":4},\
                                   "Country":{"value":"India","order":5}},"order":7},\
                    "Phone":{"value":123456789,"order":9},"AltPhone":{"value":9876543210,"order":10},"Fax":{"value":987651234,"order":11},\
                    "Email":{"value":"aaa@gmail.com","order":12},"Cc":{"value":"bbb@gmail.com","order":13},\
                    #"Contact":{"value":"Binny","order":14},\
                    #"AdditionalContactRef":{"value":{"ContactName":{"value":"Mobile","order":1},"ContactValue":{"value":"36423793","order":2}},"order":15},\
                    "Contacts":{"value":{"Salutation":{"value":"Mr","order":1},"FirstName":{"value":"Binny","order":2},\
                                         "MiddleName ":{"value":"","order":3},"LastName":{"value":"Abraham","order":4},\
                                         "JobTitle":{"value":"Software Engineer","order":5}},\
                                "order":14},\
                    }}
            xml_req = generate_req_xml(tag_dct)
        elif req_type == 'ItemService':
            service_data = [get_random_name(5), get_random_name(), req_type, 25.0]
            xml_req = generate_service_add_req(service_data)
        elif req_type == 'SalesOrder':
            full_name_tag = 'RefNumber'
            list_id_tag = 'TxnID'
            sale_id = get_random_name()
            ref_number = get_random_name()
            customer_id = '80001D37-1409135140'
            xml_req = generate_saleorder_add_req([sale_id, customer_id, ref_number])
        elif req_type == 'Invoice':
            full_name_tag = 'RefNumber'
            list_id_tag = 'TxnID'
            inv_id = get_random_name()
            customer_id = '80001D38-1409135141'
            xml_req = generate_invoice_add_req([inv_id, customer_id])
        reference_id = 'RAN' + req_type[:3].upper() + str(counter)
        quick_book_insertion(lgr, xml_req, counter, base_tag, full_name_tag, list_id_tag, entity_type, reference_id)#, csv_writer)
    lgr.info(" ::: INSERTION ENDS ::: ")

def get_random_name(num=6):
    ran_str_lst = []
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'

    for k in range(1, num+1):
        ran_str_lst.append(random.choice(chars))

    return "".join(ran_str_lst)
 
def write_to_csv(record, csv_writer):
    try:
        csv_writer.writerow(record)
    except Exception,e:
        print e
        lgr.error(str(e))

def get_csv_writer(filename):

    csv_file = open(filename,'wb')
    csv_writer = csv.writer(csv_file)
    header = ['Sequence','Time Stamp','Entity Type','Duration (ms)']
    csv_writer.writerow(header)
    return csv_writer

def get_data(entity_type):

    if entity_type == 'CustomerAccount':
        sql_query = "select c.object_ptr_id, c.name, c.phone, c.mobile, c.fax, c.email1, c.email2, c.website, \
                        b.bill_city, b.bill_code, b.bill_state, b.bill_country, b.bill_street, b.bill_pobox, \
                        s.ship_city, s.ship_code, s.ship_state, s.ship_country, s.ship_street, s.ship_pobox \
                 from customer_customercontact c, customer_accountbillads b, customer_accountshipads s \
                 where c.object_ptr_id = b.accountaddressid_id and c.object_ptr_id = s.accountaddressid_id ;"
    elif entity_type == 'Service':
        sql_query = "select object_ptr_id, name, product_type, sell_price from sales_product;"
    elif entity_type == 'SaleOrder':
        sql_query = "select s.object_ptr_id, s.reference, s.createddate, o.customeraccount_id from sales_saleorder s, sales_opportunity o where s.opportunity_id = o.object_ptr_id;"
    elif entity_type == 'Invoice':
        sql_query = "select i.object_ptr_id, i.product_id, i.quantity, i.rate, i.order_id, o.customeraccount_id from sales_orderedproduct i, sales_saleorder s, sales_opportunity o where i.order_id = s.object_ptr_id and s.opportunity_id = o.object_ptr_id;"

    crm_data = get_data_from_crm(sql_query)
    return crm_data
        
    
   

if __name__ == "__main__":

#     import sys
#     if len(sys.argv)>1:
#         param = sys.argv[1]
#         quick_book_random(param)
#     else:
#         print "Enter the name of service to execute"

    qbxml_query = """<?xml version="1.0" encoding="utf-8"?>
         <?qbxml version="13.0"?>
         <QBXML>
             <QBXMLMsgsRq onError="stopOnError">
                 <CustomerAddRq>
                     <CustomerAdd> <!-- required -->
                         <Name>Travel Software Solutions</Name> <!-- required -->
                         <IsActive>1</IsActive> <!-- optional -->
                         <CompanyName>Travel Software</CompanyName> <!-- optional -->
                         <Salutation>Mr</Salutation> <!-- optional -->
                         <FirstName>Jack</FirstName> <!-- optional -->
                         <MiddleName>W</MiddleName> <!-- optional -->
                         <LastName>Joe</LastName> <!-- optional -->
                         <JobTitle>Manager</JobTitle>
                         <Contact>Mr Binny Abraham</Contact>
                         <AltContact>Mr Ali Khalil</AltContact>
                         <AdditionalContactRef>
                             <ContactName>Main Phone</ContactName>
                             <ContactValue>973 36423793</ContactValue>
                         </AdditionalContactRef>
                         <AdditionalContactRef>
                             <ContactName>Work Phone</ContactName>
                             <ContactValue>435435345</ContactValue>
                         </AdditionalContactRef>
                         <AdditionalContactRef>
                             <ContactName>Mobile</ContactName>
                             <ContactValue>435435345435</ContactValue>
                         </AdditionalContactRef>
                         <AdditionalContactRef>
                             <ContactName>Fax</ContactName>
                             <ContactValue>973 1724 3793</ContactValue>
                         </AdditionalContactRef>
                         <AdditionalContactRef>
                             <ContactName>Main Email</ContactName>
                             <ContactValue>ram@raaa.com</ContactValue>
                         </AdditionalContactRef>
                         <AdditionalContactRef>
                             <ContactName>CC Email</ContactName>
                             <ContactValue>aaa@aaa.com</ContactValue>
                         </AdditionalContactRef>
                         <AdditionalContactRef>
                             <ContactName>Website</ContactName>
                             <ContactValue>www.web.com</ContactValue>
                         </AdditionalContactRef>
                         <AdditionalContactRef>
                             <ContactName>Other 1</ContactName>
                             <ContactValue>www.travel.com</ContactValue>
                         </AdditionalContactRef>
                         <Contacts> <!-- optional, may repeat -->
                             <Salutation>Mr</Salutation> <!-- optional -->
                             <FirstName>Binny</FirstName> <!-- required -->
                             <LastName>Abraham</LastName> <!-- optional -->
                             <JobTitle>Developer</JobTitle> <!-- optional -->
                             <AdditionalContactRef>
                                 <ContactName>Work Phone</ContactName>
                                 <ContactValue>973 36423793</ContactValue>
                             </AdditionalContactRef>
                             <AdditionalContactRef>
                                 <ContactName>Work Fax</ContactName>
                                 <ContactValue>435435345</ContactValue>
                             </AdditionalContactRef>
                             <AdditionalContactRef>
                                 <ContactName>Mobile</ContactName>
                                 <ContactValue>435435345435</ContactValue>
                             </AdditionalContactRef>
                             <AdditionalContactRef>
                                 <ContactName>Main Email</ContactName>
                                 <ContactValue>ram@raaa.com</ContactValue>
                             </AdditionalContactRef>
                             <AdditionalContactRef>
                                 <ContactName>Additional Email</ContactName>
                                 <ContactValue>bbb@raaa.com</ContactValue>
                             </AdditionalContactRef>
                         </Contacts>
                     </CustomerAdd>
                 </CustomerAddRq>
             </QBXMLMsgsRq>
         </QBXML>"""

##    qbxml_query = """<?xml version="1.0" encoding="utf-8"?>
##        <?qbxml version="13.0"?>
##        <QBXML>
##            <QBXMLMsgsRq onError="stopOnError">
##                <ItemQueryRq>
##                    <!-- BEGIN OR -->
##                    <ListID>236232377676</ListID> <!-- optional, may repeat -->
##                </ItemQueryRq>
##            </QBXMLMsgsRq>
##        </QBXML>"""

    from quick_books import quick_book_connector

    print quick_book_connector(qbxml_query)
