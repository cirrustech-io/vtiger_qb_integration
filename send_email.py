#!/usr/bin/env python
""" 
==============================================================================
title           : invoice_mailer.py
description     : This file is used to send the attachment mails 
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python invoice_mailer.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import smtplib
# For guessing MIME type
import mimetypes
# Import the email modules we'll need
import email
import email.mime.application
from qbcrm_config import smtp_host, smtp_port, sender, default_mail_lst
import json

def email_generator(email_subject, email_text, reciever_mail_lst=None, attachment_flag=None):

    reciever = default_mail_lst['it_department']
    if(reciever_mail_lst):
        reciever.append(reciever_mail_lst) 
  
    # Create a text/plain message
    msg = email.mime.Multipart.MIMEMultipart()
    msg['Subject'] = email_subject
    msg['From'] = sender
    msg['To'] = reciever[-1]

    # The main body is just another attachment
    body = email.mime.Text.MIMEText(email_text)
    msg.attach(body)

    if attachment_flag:
        # PDF attachment
        filename = generate_invoice_file_path(product_name, invoice_num, '.pdf')
        fp=open(filename,'rb')
        att = email.mime.application.MIMEApplication(fp.read(),_subtype="pdf")
        fp.close()
        att.add_header('Content-Disposition','attachment',filename=filename)
        msg.attach(att)

    # Send mail. I use the default port 25, but I authenticate. 
    s = smtplib.SMTP(smtp_host, smtp_port)
    s.sendmail(sender, reciever, msg.as_string())
    s.quit()

if __name__ == '__main__':

    data = {
  "_appinfo": {
    "application": "CRM_QB_Integration",
    "hostname": "DELL-PE-16.AD.2connectbahrain.com",
    "ip_address": "192.168.50.6",
    "mq": {
      "queue": "UpdateCRM",
      "vhost": "infonas_cqbe",
      "exchange": "infonas_crm"
    },
    "script": {
      "version": "1.0",
      "name": "MQ_Publisher.py",
      "description": "Publishes messages to Rabbit MQ"
    }
  },
  "payload": {
    "status": {
      "statusSeverity": "Info",
      "statusMessage": "Status OK",
      "statusCode": "0"
    },
    "name": "128941",
    "edit_sequence": "1432548989",
    "quickbooks_id": "5996B-1432544020",
    "current_balance": "",
    "crm_id": "128941",
    "txnline_id": [
      {
        "qb_txn_product_id": "80000082-1425906623",
        "crm_sequence_no": "1",
        "crm_lineitem_id": "1268",
        "qb_txn_line_id": "5996D-1432544020"
      }
    ]
  }
}

    email_generator("Error - QuickBooks Insertion Failure !!!", json.dumps(data))

