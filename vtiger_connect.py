#!/usr/bin/env python
""" 
==============================================================================
title           : test_vtiger.py
description     : This file is the main file which is used to communicate with vtiger API - stores and retrieve data from vtiger crm
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python test_vtiger.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

from WSClient import *
from qbcrm_config import vtiger_webservice_url, vtiger_user_name, vtiger_user_token

client = Vtiger_WSClient(vtiger_webservice_url)
print vtiger_webservice_url

def get_account(crm_id):

  try:
    login = get_login()

    ret_dct = {}
    if not login:
        ret_dct = {"error":"Login failed!"}
    else:
        record = crm_id
        recordInfo = get_retrieve(record)
        if recordInfo:
            ret_dct['account'] = recordInfo
            query = "SELECT * From SalesOrder WHERE account_id='%s' and sostatus = 'Active'" % (recordInfo['id'])
            ret_dct['sales_orders'] = get_query(query)
            #return json.dumps(recordInfo, sort_keys=True, indent=4, separators=(',', ': '))
        else:
            ret_dct = {"error":"Your Account doesn't exists. Try with another account"}
  except Exception,e:
      print e
      ret_dct = {"error":"An error in login. Please try again"}
  return ret_dct

def get_all_accounts(start):

  try:
    login = get_login()

    ret_dct = {}
    if not login:
        ret_dct = {"error":"Login failed!"}
    else:
        query = "SELECT * From Accounts LIMIT %s, 100;" % (str(start))
        ret_dct['accounts'] = get_query(query)
  except Exception,e:
        print e
        ret_dct = {"error":"An error in login. Please try again"}
  return ret_dct
    
def get_login():
    return client.doLogin(vtiger_user_name, vtiger_user_token)

def get_retrieve(record):
    get_login()
    return client.doRetrieve(record)

def get_query(query):
    get_login()
    return client.doQuery(query)

def get_describe(module):
    get_login()
    return client.doDescribe(module)

def get_create(module, record):
    get_login()
    return client.doCreate(module, record)

def get_invoke(method, record):
    get_login()
    return client.doInvoke(method, record)

def get_update(module, record):
    get_login()
    return client.doUpdate(module, record)

