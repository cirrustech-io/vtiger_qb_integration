#!/usr/bin/env python
""" 
==============================================================================
title           : qbcrm_helper.py
description     : This is an helper files to write the supporting functionalities of the integration
author          : Binny Abraham
date            : 20150526
version         : 1.0
usage           : python qbcrm_helper.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

from qbcrm_config import qb_logger_name, qb_logger_path, crm_module_mapping
from vtiger_connect import get_retrieve, get_update, get_query
from db_connector import get_db_query
import logging

"""Global Variables"""
sale_type_key = crm_module_mapping['salesorder']['sale_type_tag']
invoice_start_date_key = crm_module_mapping['salesorder']['invoice_start_date_tag']
invoice_stop_date_key = crm_module_mapping['salesorder']['invoice_stop_date_tag']
total_amt_key = 'hdnGrandTotal'
opp_group_key = crm_module_mapping['potential']['opp_group_tag']
inv_ref_no_key = crm_module_mapping['invoice']['reference_no_tag']
inv_opp_group_key = crm_module_mapping['invoice']['opp_group_tag']
prod_qb_id_key = crm_module_mapping['product']['quickbooks_id_tag']
circuit_id_key = crm_module_mapping['potential']['circuit_id_tag']

"""Billing Address fields"""
bill_flat_key = crm_module_mapping['potential']['billing_address']['flat_tag']
bill_building_key = crm_module_mapping['potential']['billing_address']['building_tag']
bill_road_key = crm_module_mapping['potential']['billing_address']['road_tag']
bill_block_key = crm_module_mapping['potential']['billing_address']['block_tag']
bill_postal_key = crm_module_mapping['potential']['billing_address']['postal_tag']
bill_city_key = crm_module_mapping['potential']['billing_address']['city_tag']
bill_state_key = crm_module_mapping['potential']['billing_address']['state_tag']
bill_country_key = crm_module_mapping['potential']['billing_address']['country_tag']

def get_data_logger():
    # create logger
    lgr = logging.getLogger(qb_logger_name)
    lgr.setLevel(logging.INFO)
    
    # add a file handler
    fh = logging.FileHandler(qb_logger_path)
    fh.setLevel(logging.INFO)
    
    # create a formatter and set the formatter for the handler.
    frmt = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(frmt)
    
    # add the Handler to the logger
    lgr.addHandler(fh)
    return lgr

def get_reference_number(entity_id, entity_type, ref_entity_id, ref_entity_type):

    ref_no_key = crm_module_mapping[ref_entity_type]['reference_no_tag']
    ref_table = crm_module_mapping[ref_entity_type]['table_name']

    ent_id = add_tabid(entity_id, entity_type)
    entity_data = get_retrieve(ent_id)
    reference_no = 1
    if entity_data.has_key(opp_group_key) and (entity_data[opp_group_key]):
        ref_no_query = "SELECT MAX(%s) FROM %s;" % (ref_table, ref_no_key)
        ref_no = get_db_query(ref_no_query, 'select')
        if ref_no:
            reference_no = int(ref_no[0][0]) + 1
    else:
        reference_no = int(remove_tabid(ref_entity_id))

    return reference_no

def remove_tabid(entity_id):
    """
    Function to remove tabid from the entity ids
    """
    try:
        entity_id = entity_id.split('x')[1]
    except:
        pass
    return entity_id

def add_tabid(entity_id, entity_type):
    """
    Function to add tabid
    """
    return crm_module_mapping[entity_type]['tab_id'] + entity_id.strip().replace(crm_module_mapping[entity_type]['tab_id'])
   
def get_potential_group():
    """
    Function to get all opportunity group
    """
    opp_query = "SELECT opportunitygroupid FROM vtiger_opportunitygroup;"
    opp_data = get_db_query(opp_query, 'select')
    return opp_data
 
