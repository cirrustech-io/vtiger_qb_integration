#!/usr/bin/env python
""" 
==============================================================================
title           : rmqConnection.py
description     : This file is the rabbitmq connection file
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python rmqConnection.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import pika
from rmqConfig import VIRTUAL_HOST, HOST_NAME

class connection:

    def __init__(self):
        #self.username = USER_NAME
        #self.password = PASS_WORD
        self.host = HOST_NAME
        self.virtualhost = VIRTUAL_HOST
        

    def get_connection(self):
        #credentials = pika.PlainCredentials(self.username, self.password)
        conn_params = pika.ConnectionParameters(host=self.host, virtual_host=self.virtualhost)
        connection = pika.BlockingConnection(conn_params)
        return connection
   
