#!/usr/bin/env python
""" 
==============================================================================
title           : test_vtiger.py
description     : This file is used to test vtiger apis
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python test_vtiger.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

from vtiger_connect import *
import sys

rec_id = sys.argv[1]
print get_retrieve(rec_id)
