#!/usr/bin/env python
""" 
==============================================================================
title           : pylabtest.py
description     : This is used to generate pdf using pylab module
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python pylabtest.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

from pyPdf import PdfFileWriter, PdfFileReader
import StringIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter

packet = StringIO.StringIO()
# create a new PDF with Reportlab
can = canvas.Canvas(packet, pagesize=letter)
can.drawString(60, 465, "2")
can.drawString(117, 465, "Hello world")
can.save()

#move to the beginning of the StringIO buffer
packet.seek(0)
new_pdf = PdfFileReader(packet)
# read your existing PDF
existing_pdf = PdfFileReader(file("invoice_org.pdf", "rb"))
output = PdfFileWriter()
# add the "watermark" (which is the new pdf) on the existing page
page = existing_pdf.getPage(0)
page.mergePage(new_pdf.getPage(0))
output.addPage(page)
# finally, write "output" to a real file
outputStream = file("invoice_dest.pdf", "wb")
output.write(outputStream)
outputStream.close()