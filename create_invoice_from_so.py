from db_connector import get_db_query
from vtiger_connect import get_retrieve, get_create
from datetime import datetime, timedelta
from qbcrm_config import crm_module_mapping
from qbcrm_helper import *
from publisher import publish_to_queue
from invoice_partial_calculation import get_partial_amount, get_date, get_total_days_from_month

#
# Function to create invoice outside
# param - salesorder_id Salesorder ID
# param - invoice_date Invoice Date
#
def create_Invoice_From_SO(salesorder_id, invoice_date):

    sale_id = crm_module_mapping['salesorder']['tab_id']+salesorder_id
    sales_dct = get_retrieve(sale_id)

    generate_invoice(sales_dct, salesorder_id, invoice_date)

def generate_inventory(sales_dct, salesorder_id, invoice_id, inventory_type, invoice_date, partial_amount):

    """
    Module to create the inventory to generate the line items for the generated invoice
    """

    if inventory_type == 'main':
        query1 = "SELECT * FROM vtiger_inventoryproductrel WHERE id=%s;" % (salesorder_id)
    elif inventory_type == 'sub':
        query1 = "SELECT * FROM vtiger_inventorysubproductrel WHERE id=%s;" % (salesorder_id)
    else:
        query1 = "SELECT * FROM vtiger_inventoryshippingrel WHERE id=%s;" % (salesorder_id)

    inventory_res = get_db_query(query1, 'column')
    
    seq_num = 0
    if sales_dct['sostatus'] == 'Delivered':
        #Create line item and insert in to inventory table for normal line items from sales order
        seq_num = create_line_item(sales_dct, inventory_res, invoice_id, inventory_type)

    #Create line item and insert in to inventory table for partial amount
    if partial_amount and inventory_type == 'main':
        create_line_item(sales_dct, inventory_res, invoice_id, inventory_type,  partial_amount, seq_num)

def create_line_item(sales_dct, inventory_res, invoice_id, inventory_type, partial_amount=None, seq_num=None):

    seq_no = 0
    if seq_num:
        seq_no = seq_num

    for row in inventory_res:

        insert_flag = True
        nrc_flag = 0
        #delete so lineitem id. It will automatically create new lineitem id
        if row.has_key('lineitem_id'):
            del row['lineitem_id']
        
        #set integer values to 0 if value is None
        if row.has_key('incrementondel'):
            if not row['incrementondel']:
                row['incrementondel'] = 0

        """set decimal values to 0.0 if value is None"""
        flt_keys = ['tax1', 'tax2', 'tax3', 'shtax1', 'shtax2', 'shtax3', 'discount_amount', 'discount_percent']
        row = convert_to_float(row, flt_keys)

        """set row id to invoice id"""
        row['id'] = invoice_id
       
        """ Check whether the product is non recurring or not and set the flag""" 
        if row.has_key('productid'):
            nrc_flag = check_for_nrc(int(row['productid']))
            if nrc_flag:
                insert_flag = False

        invoice_start_date = sales_dct[invoice_start_date_key]
        """Get Invoice period"""
        invoice_period_month, invoice_start_period, invoice_end_period = get_invoice_period(invoice_date)

        if(partial_amount):

            """Get Invoice period"""
            invoice_period_month, invoice_start_period, invoice_end_period = get_invoice_period(invoice_start_date)
 
            """Set listprice according to each line item partial amount"""
            if nrc_flag:
                list_partial_amount = float(row['listprice'])
                insert_flag = True 
            else:
                list_partial_amount = get_partial_amount(invoice_start_date, invoice_date, float(row['listprice']))

            row['listprice'] = str(list_partial_amount)
            invoice_month = (get_date(invoice_start_date, '%Y-%m-%d') - timedelta(days=5)).strftime("%B %Y")

        """Set the invoice comment accordingly"""
        if (row.has_key('comment')):
            if nrc_flag and insert_flag:
                row['comment'] = "One time Non Recurring Charge"
            else:
                cmnt_start = 'Invoice'
                if sales_dct['sostatus'] != 'Delivered':
                    cmnt_start = 'Credit Invoice'
                    
                row['comment'] = '%s Amount for the period of (%s to %s)' % (cmnt_start, invoice_start_period, invoice_end_period)

        #Prepare Insert queries
        columns = row.keys()
        values = row.values()
        row_values = "','".join([str(item) if item!=None else '' for item in values])
        if inventory_type == 'main':
            query2 = "INSERT INTO vtiger_inventoryproductrel("+ ",".join(columns) +") VALUES ('"+ row_values +"');"
        elif inventory_type == 'sub':
            query2 = "INSERT INTO vtiger_inventorysubproductrel("+ ",".join(columns) +") VALUES ('"+ row_values +"');"
        elif inventory_type == 'shipping':
            query2 = "INSERT INTO vtiger_inventoryshippingrel("+ ",".join(columns) +") VALUES ('"+ row_values +"');"

        # Insert into inventory table only if insert flag is True
        if insert_flag:
            
            # Set sequence number of the rows
            if row.has_key('sequence_no'):
                seq_no += 1
                row['sequence_no'] = seq_no
            
            print row
            get_db_query(query2, 'insert')
        #updateStk(row['productid'], row['quantity'], '', [], 'Invoice') 
    return seq_no

def generate_partial_amount(sales_dct, invoice_date, mrc_amount):

    invoice_start_date = sales_dct[invoice_start_date_key]
    sale_type = get_sale_type(sales_dct)
    credit_amount = 0.0
    partial_amount = 0.0
    cease_flag = False
    prev_sales_dct = {}
    
    if (sale_type == 'cease'):
       
        """if credit note invoice stop date should send to partial amount as its calculated according to invoice stop date and issue credit note"""
        credit_amount = get_partial_amount(sales_dct[invoice_stop_date_key], invoice_date, mrc_amount)
        cease_flag = True
        print "CREDIT AMOUNT ON CEASE :::: '%s'" % (credit_amount)
    elif (sale_type == 'upgrade' or sale_type == 'downgrade' or sale_type == 'pricechange'):
        
        """Find partial amount"""
        partial_amount = get_partial_amount(invoice_start_date, invoice_date, mrc_amount)

        """Find credit amount from previous inactive sales order and issue credit note"""
        prev_sales_dct = get_prev_sales_order(sales_dct)
        mrc_amount = float(prev_sales_dct[total_amt_key])

        """if credit note invoice stop date should send to partial amount as its calculated according to invoice stop date and issue credit note"""
        credit_amount = get_partial_amount(prev_sales_dct[invoice_stop_date_key], invoice_date, mrc_amount)
        print "CREDIT AMOUNT DURING UPGRADE :::: '%s'" % (credit_amount)
    else:
        """Find partial amount"""
        partial_amount = get_partial_amount(invoice_start_date, invoice_date, mrc_amount)

    return partial_amount, credit_amount, cease_flag, prev_sales_dct

def get_invoice_period(dte):

    """Get Invoice period"""
    inv_prd_dte = get_date(dte, '%Y-%m-%d')
    invoice_period_month = inv_prd_dte.strftime("%B %Y")
    invoice_start_period = inv_prd_dte.strftime(str(inv_prd_dte.day)+" %b %Y")
    invoice_end_period = inv_prd_dte.strftime(str(get_total_days_from_month(inv_prd_dte.year, inv_prd_dte.month)[1])+" %b %Y")
    return invoice_period_month, invoice_start_period, invoice_end_period 

def check_for_nrc(product_id):
    nrc_flag = 0
    prd_qry = 'select cf_1049 from vtiger_productcf where productid = %s;' %(product_id)
    rc_val = get_db_query(prd_qry, 'select')
    print rc_val
    if(not int(rc_val[0][0])):
        nrc_flag = 1
    return nrc_flag

def convert_to_float(row, flt_keys):

    #set decimal values to 0.0 if value is None
    for fltk in flt_keys:
        if row.has_key(fltk):
            if not row[fltk]:
                row[fltk] = '0.0'
    return row

def generate_invoice(sales_dct, salesorder_id, invoice_date):

    """
    Module to generate invoice from salesorder details    
    """

    total_amount = float(sales_dct[total_amt_key])
    partial_amount, credit_amount, cease_flag, prev_sales_dct = generate_partial_amount(sales_dct, invoice_date, total_amount)
    invoice_status = 'AutoCreated'
    total_invoice_amount = total_amount + partial_amount
    
    if cease_flag:

        invoice_status = 'Credit Invoice'
        total_invoice_amount = total_amount + credit_amount
        partial_amount = credit_amount

    invoice_insertion(sales_dct, salesorder_id, invoice_date, partial_amount, total_invoice_amount, invoice_status)

    if prev_sales_dct:
        invoice_status = 'Credit Invoice'

        total_invoice_amount = total_amount + credit_amount
        partial_amount = credit_amount

        invoice_insertion(prev_sales_dct, remove_tabid(prev_sales_dct['id']), invoice_date, partial_amount, total_invoice_amount, invoice_status)

def invoice_insertion(sales_dct, salesorder_id, invoice_date, partial_amount, total_invoice_amount, invoice_status):


    print partial_amount
    id_query = "SELECT MAX(crmid) FROM vtiger_crmentity";
    invoice_id = int(get_db_query(id_query, 'select')[0][0]) + 1
    print invoice_id

    due_date = (datetime.strptime(invoice_date, '%Y-%m-%d') + timedelta(days=30)).strftime("%Y-%m-%d")
    created_tme = datetime.today().strftime("%Y-%m-%d %X")

    max_inv_id_query = "SELECT MAX(invoiceid) FROM vtiger_invoice;"
    max_inv_id = get_db_query(max_inv_id_query, 'select')
    
    inv_no_query = "SELECT invoice_no FROM vtiger_invoice WHERE invoiceid = %s;" % (max_inv_id[0][0])
    invoice_no = "INV"+str(int(get_db_query(inv_no_query, 'select')[0][0].split("INV")[1]) + 1)
    print invoice_status

    invoice_dct = {
            'accountid':remove_tabid(sales_dct['account_id']),
            'contactid': remove_tabid(sales_dct['contact_id']),
            'salesorderid': salesorder_id,
            'invoiceid':str(invoice_id),
            'invoice_no':invoice_no,
            'subject': sales_dct['subject'],
            'invoicestatus': invoice_status,
            'invoicedate':invoice_date,
            'duedate':due_date,
            'taxtype':sales_dct['hdnTaxType'],
            'discount_amount':sales_dct.get('hdnDiscountAmount','0.0'),
            'adjustment': sales_dct.get('txtAdjustment','0.0'),
            'salescommission': sales_dct.get('salescommission','0.0'),
            'exciseduty': sales_dct.get('exciseduty','0.0'),
            'subtotal': str(total_invoice_amount),
            'total': str(total_invoice_amount),
            'discount_percent': sales_dct.get('hdnS_H_Percent','0.0'),
            's_h_amount': sales_dct.get('hdnS_H_Amount','0.0'),
            'terms_conditions':sales_dct.get('terms_conditions','0.0'),
            'pre_tax_total':sales_dct.get('pre_tax_total','0.0'),
            's_h_percent':sales_dct.get('hdnS_H_Percent','0.0'),
            'purchaseorder':sales_dct.get('vtiger_purchaseorder','0.0'),
    }

    """set decimal values to 0.0 if value is None"""
    flt_keys = ['salescommission', 'exciseduty', 'discount_amount', 'adjustment', 'discount_percent', 's_h_amount', 'pre_tax_total', 's_h_percent']
    invoice_dct = convert_to_float(invoice_dct, flt_keys)

    print invoice_dct
    inv_columns = ",".join(invoice_dct.keys())
    inv_values = "','".join(invoice_dct.values())
    inv_query = "INSERT INTO vtiger_invoice("+ inv_columns +") VALUES ('"+ inv_values +"');"
    get_db_query(inv_query, 'insert')
    
    invoice_crmentity_dct = {
        'crmid': str(invoice_id),
        'smcreatorid': '1',
        'smownerid': '1',
        'modifiedby': '1',
        'setype': 'Invoice',
        'description': sales_dct['description'],
        'createdtime': created_tme,
        'modifiedtime': created_tme,
        'label': sales_dct['subject'],
    }
   
    crm_columns = invoice_crmentity_dct.keys()
    crm_values = invoice_crmentity_dct.values()
    crm_query = "INSERT INTO vtiger_crmentity("+ ",".join(crm_columns) +") VALUES ('"+ "','".join(crm_values) +"');"
    get_db_query(crm_query, 'insert')

    """Get OpportunityGroupID"""
    inv_opp_group_id = get_opp_group_id(sales_dct['potential_id'])
    invcf_query = "INSERT INTO vtiger_invoicecf(invoiceid, %s) VALUES (%s, '%s');" % (inv_opp_group_key, invoice_id, inv_opp_group_id)

    """Get Billing Address Details"""
    bill_dct = get_billing_address(sales_dct['potential_id'])

    bill_query = "INSERT INTO vtiger_invoicebillads(invoicebilladdressid, bill_city, bill_code, bill_country, bill_state, bill_street, bill_pobox) VALUES ('%s', '%s', '%s', '%s', '%s', '%s, %s, %s, %s', '%s');" % (invoice_id, bill_dct['bill_city'], bill_dct['bill_postal'], bill_dct['bill_country'], bill_dct['bill_state'], bill_dct['bill_flat'], bill_dct['bill_building'], bill_dct['bill_road'], bill_dct['bill_block'], bill_dct['bill_postal'])
    print bill_query

    ship_query = "INSERT INTO vtiger_invoiceshipads(invoiceshipaddressid, ship_city, ship_code, ship_country, ship_state, ship_street, ship_pobox) VALUES ('%s' , '%s', '%s', '%s', '%s', '%s, %s, %s, %s', '%s');" % (invoice_id, bill_dct['bill_city'], bill_dct['bill_postal'], bill_dct['bill_country'], bill_dct['bill_state'], bill_dct['bill_flat'], bill_dct['bill_building'], bill_dct['bill_road'], bill_dct['bill_block'], bill_dct['bill_postal'])
    print ship_query
    
    get_db_query(invcf_query, 'insert')

    """Create reference number which act as the invoice number for QB - To accomplish Consolidate Invoice"""
    ref_num = get_invoice_qb_reference_number(invoice_date, inv_opp_group_id, invoice_status)
    ref_up_query = "UPDATE vtiger_invoicecf SET %s = %s WHERE invoiceid = %s;" % (inv_ref_no_key, ref_num, invoice_id)
    print ref_up_query
    get_db_query(ref_up_query, 'update')

    get_db_query(bill_query, 'insert')
    get_db_query(ship_query, 'insert')

    generate_inventory(sales_dct, salesorder_id, invoice_id, "main", invoice_date, partial_amount)
    generate_inventory(sales_dct, salesorder_id, invoice_id, "sub", invoice_date, partial_amount)
    generate_inventory(sales_dct, salesorder_id, invoice_id, "shipping", invoice_date, partial_amount)

    push_data_to_queue(invoice_id)

def push_data_to_queue(invoice_id):
    
    queue_dct = {} 
    inv_id = crm_module_mapping['invoice']['tab_id']+str(invoice_id)
    inv_data = get_retrieve(inv_id)
    if inv_data:
        sale_data = get_retrieve(inv_data['salesorder_id'])
        con_data = get_retrieve(inv_data['contact_id'])
        acc_data = get_retrieve(inv_data['account_id'])
        pot_data = get_retrieve(sale_data['potential_id'])

        for line_item in inv_data['LineItems']:
            line_item['product_entity'] = get_retrieve(line_item['productid'])

        queue_name = 'NewInvoice'
        entity_name = 'invoice'
        if (inv_data.has_key('invoicestatus') and inv_data['invoicestatus'] == 'Credit Invoice'):
            queue_name = 'NewCreditMemo'
            entity_name = 'creditmemo'

        queue_dct = {'account':acc_data, 'potential':pot_data, 'contact':con_data, 'salesorder':sale_data, entity_name:inv_data}
        publish_to_queue(queue_dct, queue_name)

def get_opp_group_id(pot_id):

    grp_id = ''
    grp_qry = "SELECT %s from vtiger_potentialscf WHERE potentialid = %s;" % (opp_group_key, remove_tabid(pot_id))
    grp = get_db_query(grp_qry, 'select')
    if grp:
        grp_id = grp[0][0]
    return grp_id

def get_invoice_qb_reference_number(invoice_date, opp_grp_id, invoice_status):

    reference_no = 1
    if opp_grp_id:
        ref_no_query = "SELECT MIN(vtiger_invoice.invoiceid) from vtiger_invoice, vtiger_invoicecf, vtiger_crmentity WHERE vtiger_invoicecf.%s = %s AND vtiger_invoice.invoiceid = vtiger_invoicecf.invoiceid AND vtiger_crmentity.crmid = vtiger_invoice.invoiceid AND vtiger_crmentity.deleted = 0 AND vtiger_invoice.invoicedate = '%s' AND vtiger_invoice.invoicestatus = '%s';" % (inv_opp_group_key, opp_grp_id, invoice_date, invoice_status)
        print ref_no_query
        ref_no = get_db_query(ref_no_query, 'select')
        if ref_no:
            reference_no = int(ref_no[0][0])
    else:
        reference_no = int(remove_tabid(ref_entity_id))

    return reference_no

def get_billing_address(potential_id):
    
    pot_dct = get_retrieve(potential_id)
    return {'bill_flat':pot_dct[bill_flat_key], 'bill_building':pot_dct[bill_building_key], 'bill_road':pot_dct[bill_road_key],
            'bill_block':pot_dct[bill_block_key], 'bill_postal':pot_dct[bill_postal_key], 'bill_city':pot_dct[bill_city_key],
            'bill_state':pot_dct[bill_state_key], 'bill_country':pot_dct[bill_country_key]}
    
def get_shipping_address(potential_id):
    pass

def get_sale_type(sales_dct):

    """
    Function to get the sale type of a saleorder
    """

    sale_type = 'new'
    if (sales_dct.has_key(sale_type_key) and sales_dct[sale_type_key]):
        sale_type = sales_dct[sale_type_key].lower()
    return sale_type

def get_prev_sales_order(sales_dct):

    """
    Function to get the inactive or previous saleorder using the current active salesorderid
    """

    potentialid = remove_tabid(sales_dct['potential_id'])
    prev_sale_query = "SELECT sale.salesorderid from vtiger_salesorder sale, vtiger_crmentity crm WHERE sale.potentialid = %s and sale.sostatus='inactive' AND sale.salesorderid = crm.crmid AND crm.deleted=0;" % (potentialid)
    prev_salesorder_id = get_db_query(prev_sale_query, 'select')[0][0]

    prev_sale_id = crm_module_mapping['salesorder']['tab_id']+str(prev_salesorder_id)
    prev_sales_dct = get_retrieve(prev_sale_id)
    return prev_sales_dct


if __name__ == '__main__':
   
    import sys
    so_id = sys.argv[1]
    invoice_date = sys.argv[2]
    create_Invoice_From_SO(so_id, invoice_date)

