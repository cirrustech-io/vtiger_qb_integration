#!/usr/bin/env python
""" 
==============================================================================
title           : db_config.py
description     : This is the configuration file for vtiger database connection.
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python db_config.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""
import os

if os.name == 'posix':
    crm_host = 'localhost'
else:
    crm_host = '192.168.2.41'
crm_user = 'root'
crm_passwd = '3x1651a'
crm_db = 'infonas_vtiger610'
