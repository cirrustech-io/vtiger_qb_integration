#!/usr/bin/env python
""" 
==============================================================================
title           : publisher.py
description     : This is the publisher file which publish messages to rabbitmq
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python publisher.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

import json
import socket

host_name = socket.getfqdn()
ip_address = socket.gethostbyname(host_name)

def publish_to_queue(resp, code):

    data = {
          "_appinfo": {
            "application": "CRM_QB_Integration",
            "script": {
              "name": "MQ_Publisher.py",
              "description": "Publishes messages to Rabbit MQ",
              "version": "1.0"
            },
            "hostname": host_name,
            "ip_address": ip_address,
            "mq": {
              "vhost": "infonas_cqbe",
              "exchange": "infonas_crm",
              "queue": "UpdateCRM"
            }
          },
          "payload": resp
    }
    message = json.dumps(data)
    from rmqTask import Task
    task = Task()
    
    task.publish_message(message, code)
    task.close_connection()
