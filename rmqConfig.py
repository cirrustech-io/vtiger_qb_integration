#!/usr/bin/env python
""" 
==============================================================================
title           : rmqConfig.py
description     : This file is the rabbitmq configuration file
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python rmqConfig.py
notes           : 
python_version  : 2.7.9
==============================================================================
"""

VIRTUAL_HOST = 'infonas_crm'
HOST_NAME = 'mq.2connectbahrain.com'
MAIN_EXCHANGE = {'name':'infonas_cqbe', 'type':'topic'}

QUEUE_MAP = {'workflow_new_account':{'queue_name':'UpdateQB','type':'direct', 'routing_key':'workflow.new_account'},
             'workflow_update_account':{'queue_name':'UpdateQB','type':'direct', 'routing_key':'workflow.update_account'},
             'qb_new_account':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.new_account'},
             'qb_update_account':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.update_account'},
             
             'workflow_new_potential':{'queue_name':'UpdateQB','type':'direct', 'routing_key':'workflow.new_potential'},
             'workflow_update_potential':{'queue_name':'UpdateQB','type':'direct', 'routing_key':'workflow.update_potential'},
             'qb_new_potential':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.new_potential'},
             'qb_update_potential':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.update_potential'},
             
             'workflow_new_salesorder':{'queue_name':'UpdateQB','type':'direct', 'routing_key':'workflow.new_salesorder'},
             'workflow_update_salesorder':{'queue_name':'UpdateQB','type':'direct', 'routing_key':'workflow.update_salesorder'},
             'qb_new_salesorder':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.new_salesorder'},
             'qb_update_salesorder':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.update_salesorder'},
             
             'workflow_new_product':{'queue_name':'UpdateQB','type':'direct', 'routing_key':'workflow.new_product'},
             'workflow_update_product':{'queue_name':'UpdateQB','type':'direct', 'routing_key':'workflow.update_product'},
             'QB_ProductCreateUpdate':{'queue_name':'QB_ProductCreateUpdate','type':'direct', 'routing_key':'workflow.product.CreateUpdate'},
             'qb_new_product':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.new_product'},
             'qb_update_product':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.update_product'},
             
             'NewInvoice':{'queue_name':'NewInvoice','type':'direct', 'routing_key':'workflow.new_invoice'},
             'qb_new_invoice':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.new_invoice'},
             'qb_update_invoice':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.update_invoice'},
             'InvoicePDF':{'queue_name':'InvoicePDF','type':'direct', 'routing_key':'infonas_crm.pdf_invoice'},
             'InvoiceEmail':{'queue_name':'InvoiceEmail','type':'direct', 'routing_key':'infonas_crm.email_invoice'},

             'NewCreditMemo':{'queue_name':'NewCreditMemo','type':'direct', 'routing_key':'workflow.new_creditmemo'},
             'qb_new_creditmemo':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.new_creditmemo'},
             'qb_update_creditmemo':{'queue_name':'UpdateCRM','type':'direct', 'routing_key':'qb.update_creditmemo'},
            
             'UpdateSuccess':{'queue_name':'UpdateSuccess','type':'direct', 'routing_key':'#.update_crm_success'},
             'QB_log':{'queue_name':'QB_log','type':'direct', 'routing_key':'QB_log'},
             }
