#!/usr/bin/env python
"""
==============================================================================
title           : execute_tasks.py
description     : This is the rabbitmq tasks file to execute mq tasks - Here mainly insertion and retrieve data to and fro
                  quickbooks and vtiger crm
author          : Binny Abraham
date            : 20150130
version         : 1.0
usage           : python execute_tasks.py
notes           :
python_version  : 2.7.9
==============================================================================
"""
import os
import json
if os.name != 'posix':
    from quick_books import format_qb_data, quick_book_connector, parse_response
from vtiger_connect import get_retrieve, get_update, get_query
from publisher import publish_to_queue
from invoice_generator import generate_invoice
from qbcrm_config import qb_module_mapping, crm_module_mapping
from db_connector import get_db_query
from send_email import email_generator
from qbcrm_helper import get_data_logger

logger = get_data_logger()
### Email task
def email_queue(data):

    msg_code = data['message']
    data = json.dumps(data)
    emails = (
        #(msg_code, data, 'np-test@2connectbahrain.com', ['ali.khalil@2connectbahrain.com']),
        (msg_code, data, 'np-test@2connectbahrain.com', ['binny.abraham@2connectbahrain.com']),
    )
    #results = send_mass_mail(emails)
    return emails

if os.name != 'posix':

    def connect_to_quickbooks(entity_type, action, data):

        qbxml_query = format_qb_data(entity_type, action, data)
        response_string = quick_book_connector(qbxml_query)
        fullname, listid, crmid, editsequence, current_balance, txnlineid = ('', '', '', '', '', '')

        status_dct = parse_response(entity_type, response_string, None, None)

        if((entity_type == 'account') and (action=='query')):
            current_balance = parse_response(entity_type, response_string, qb_module_mapping[entity_type]['base_tag'], qb_module_mapping[entity_type]['current_balance_tag'])

        fullname = parse_response(entity_type, response_string, qb_module_mapping[entity_type]['base_tag'], qb_module_mapping[entity_type]['full_name_tag'])
        listid = parse_response(entity_type, response_string, qb_module_mapping[entity_type]['base_tag'], qb_module_mapping[entity_type]['list_id_tag'])
        crmid = parse_response(entity_type, response_string, qb_module_mapping[entity_type]['base_tag'], qb_module_mapping[entity_type]['crm_id_tag'])
        editsequence = parse_response(entity_type, response_string, qb_module_mapping[entity_type]['base_tag'], qb_module_mapping[entity_type]['edit_sequence_tag'])

        if((entity_type == 'salesorder') or (entity_type == 'invoice') or (entity_type == 'creditmemo')):
            txnlineid = parse_response(entity_type, response_string, qb_module_mapping[entity_type]['base_tag'], qb_module_mapping[entity_type]['txn_line_id_tag'])

        resp_dct = {"status":status_dct, "name":fullname, "quickbooks_id":listid, "crm_id":crmid, "edit_sequence": editsequence, "current_balance":current_balance, "txnline_id":txnlineid}
        publish_to_queue(resp_dct, 'QB_log')
        return resp_dct

def connect_to_crm(entity_type, data):

    module = crm_module_mapping[entity_type]['module']
    crm_id = crm_module_mapping[entity_type]['tab_id'] + data['payload'][entity_type]['crm_id'].strip().replace(crm_module_mapping[entity_type]['tab_id'],'')
    quick_books_id = data['payload'][entity_type].get('quickbooks_id', crm_module_mapping[entity_type]['quickbooks_id_tag'])
    qb_edit_sequence = data['payload'][entity_type]['edit_sequence']
    crm_data = get_retrieve(crm_id)
    crm_data[crm_module_mapping[entity_type]['quickbooks_id_tag']] = quick_books_id
    crm_data[crm_module_mapping[entity_type]['edit_sequence_tag']] = qb_edit_sequence
    crm_data[crm_module_mapping[entity_type]['qb_status_tag']] = 'Completed'
    if(entity_type == 'salesorder' or entity_type == 'invoice' or entity_type == 'creditmemo'):
        qb_txnline_id = data['payload'][entity_type]['txnline_id']
        #crm_data[crm_module_mapping[entity_type]['txn_line_id_tag']] = qb_txnline_id

        new_item_lst = []
        for i in range(len(qb_txnline_id)):
            line_item = get_retrieve('33x'+qb_txnline_id[i]['crm_lineitem_id'])
            line_item['qb_txnline_id']  = qb_txnline_id[i]['qb_txn_line_id']
            sql_query = 'UPDATE vtiger_inventoryproductrel set qb_txnline_id = "%s" where lineitem_id = %s;' % (qb_txnline_id[i]['qb_txn_line_id'], qb_txnline_id[i]['crm_lineitem_id'])
            get_db_query(sql_query, 'update')
            new_item_lst.append(line_item)

        crm_data['LineItems'] = new_item_lst
        crm_data['productid'] = crm_data['LineItems'][0]['productid']
        #crm_data['invoicestatus'] = 'AutoCreated'
        #crm_data['terms_conditions'] = ''
        if(entity_type == 'invoice' or entity_type == 'creditmemo'):
            crm_data['txtAdjustment'] = data['payload'][entity_type]['current_balance']
    ret_data = get_update(module, crm_data)
    if((entity_type == 'salesorder' or entity_type == "invoice" or entity_type == "creditmemo") and (ret_data == False)):
        sql_query = 'UPDATE %s set %s = "%s", %s = "%s" where %sid = %s;' % (crm_module_mapping[entity_type]["table_name"], crm_module_mapping[entity_type]["quickbooks_id_tag"], quick_books_id, crm_module_mapping[entity_type]["edit_sequence_tag"], qb_edit_sequence, entity_type, crm_id.split('x')[1])
	print sql_query
        get_db_query(sql_query, 'update')
        ret_data = get_retrieve(crm_id)
    return ret_data

def UpdateQB(data, routing_key):

    resp_dct = {}
    entity_type = routing_key.split('_')[1]
    action = routing_key.split('.')[1].split('_')[0]

    prd_qb_id = ''
    account_qbid = ''
    potential_qbid = ''
    qb_status = 'Open'
    if data['payload'][entity_type].has_key('id'):
        data['payload'][entity_type]['record_id'] = data['payload'][entity_type].get('record_id', data['payload'][entity_type]['id']).strip().replace(crm_module_mapping[entity_type]['tab_id'],'')

    if(data['payload'].has_key('product')):
        try:
            prd_qb_id, data['payload']['product']['quickbooks_id']= data['payload']['product']['cf_847'], data['payload']['product']['cf_847']
            data['payload'][entity_type]['product_qbid'] = prd_qb_id
        except:
            pass

    if(data['payload'].has_key('account')):
        try:
            account_qbid, data['payload']['account']['quickbooks_id'] = data['payload']['account']['cf_809'], data['payload']['account']['cf_809']
            data['payload'][entity_type]['account_qbid'] = account_qbid
        except:
            pass

    if(data['payload'].has_key('potential')):
        try:
            potential_qbid, data['payload']['potential']['quickbooks_id'] = data['payload']['potential']['cf_894'], data['payload']['potential']['cf_894']
            data['payload'][entity_type]['potential_qbid'] = potential_qbid
        except:
            pass

    if not data['payload'][entity_type].has_key('quickbooks_id'):
        data['payload'][entity_type]['quickbooks_id'] = data['payload'][entity_type][crm_module_mapping[entity_type]['quickbooks_id_tag']]

    if data['payload'].has_key('salesorder'):
        for line_item in data['payload']['salesorder']['LineItems']:
            if (line_item['id'].find('x')<0):
                line_item['id'] = crm_module_mapping['salesorder']['tab_id'] + line_item['id']
                line_item['productid'] = crm_module_mapping['product']['tab_id'] + line_item['productid']

            sql_query = 'select lineitem_id from vtiger_inventoryproductrel where id=%s and productid = %s' %(line_item['id'].split('x')[1], line_item['productid'].split('x')[1])
            ret_data = get_db_query(sql_query, 'select')
            line_item['lineitem_id'] = str(ret_data[0][0])
            line_item['product_qbid'] = get_retrieve(line_item['productid'])['cf_847']

    qb_status = data['payload'][entity_type][crm_module_mapping[entity_type]['qb_status_tag']]
    if(qb_status != 'Completed'):
    	resp_dct[entity_type] = connect_to_quickbooks(entity_type, action, data)
	qb_status = 'Open'

    if(qb_status == 'Open' and entity_type=='account' and action=='new'):

        if not prd_qb_id:
            resp_dct['product'] = connect_to_quickbooks('product', action, data)
            prd_qb_id = resp_dct['product']['quickbooks_id']

        if not account_qbid:
            account_qbid = resp_dct[entity_type]['quickbooks_id']

        data['payload']['account']['quickbooks_id'] = account_qbid
        data['payload']['potential']['account_qbid'] = account_qbid
        resp_dct['potential'] = connect_to_quickbooks('potential', action, data)

        if not potential_qbid:
            potential_qbid = resp_dct['potential']['quickbooks_id']

        data['payload']['salesorder']['product_qbid'] = prd_qb_id
        data['payload']['salesorder']['account_qbid'] = account_qbid
        data['payload']['salesorder']['potential_qbid'] = potential_qbid
        resp_dct['salesorder'] = connect_to_quickbooks('salesorder', action, data)

    if(qb_status == 'Open'):
    	publish_to_queue(resp_dct, 'qb_update_'+entity_type)
    else:
        tab_name = crm_module_mapping[entity_type]['table_name']
        qb_status_tag = crm_module_mapping[entity_type]['qb_status_tag']
        crm_id = data['payload'][entity_type]['record_id']
        field_name = entity_type+'id'
        sql_query = 'UPDATE %s set %s = "Open" where %s = %s;' % (tab_name, qb_status_tag, field_name, crm_id)
        get_db_query(sql_query, 'update')

def UpdateCRM(data, routing_key):

    resp_dct = {}
    entity_type = routing_key.split('_')[1]
    action = routing_key.split('.')[1].split('_')[0]
    if(action == 'update'):
        if(data['payload'][entity_type]['quickbooks_id'] !=''):
            resp_dct[entity_type] = connect_to_crm(entity_type, data)

        if(entity_type == 'account'):
            if(data['payload'].has_key('product') and data['payload']['product']['quickbooks_id'] !=''):
                resp_dct['product'] = connect_to_crm('product', data)
	    if(data['payload'].has_key('potential') and data['payload']['potential']['quickbooks_id'] !=''):
                resp_dct['potential'] = connect_to_crm('potential', data)
            if(data['payload'].has_key('salesorder') and data['payload']['salesorder']['quickbooks_id'] !=''):
                resp_dct['salesorder'] = connect_to_crm('salesorder', data)

    #publish_to_queue(resp_dct, 'UpdateSuccess')
    #if(entity_type == 'invoice'):
    #    publish_to_queue(resp_dct, 'InvoicePDF')

def QB_ProductCreateUpdate(data, routing_key):

    entity_type = routing_key.split('.')[1] #'product'
    action = 'new' #routing_key.split('.')[1].split('_')[0]
    qb_id_tag = crm_module_mapping['product']['quickbooks_id_tag']
    edit_id_tag = crm_module_mapping['product']['edit_sequence_tag']
    if (data['payload'][entity_type].has_key(qb_id_tag) and data['payload'][entity_type][qb_id_tag]):
        action = 'update'
        data['payload'][entity_type]['quickbooks_id'] = data['payload'][entity_type][qb_id_tag]
        data['payload'][entity_type]['product_qbid'] = data['payload'][entity_type][qb_id_tag]
        data['payload'][entity_type]['edit_sequence'] = data['payload'][entity_type][edit_id_tag]
    resp_dct = {entity_type:connect_to_quickbooks(entity_type, action, data)}
    publish_to_queue(resp_dct, 'qb_update_'+entity_type)

def NewCreditMemo(data, routing_key):
    get_credit_invoice_queue(data, routing_key)

def NewInvoice(data, routing_key):
    get_credit_invoice_queue(data, routing_key)

def get_credit_invoice_queue(data, routing_key):

    resp_dct = {}

    current_balance = ''
    entity_type = routing_key.split('.')[1].split('_')[1] #'invoice'
        
    action = routing_key.split('.')[1].split('_')[0]
    
    if data['payload'][entity_type].has_key('id'):
        data['payload'][entity_type]['record_id'] = data['payload'][entity_type].get('record_id', data['payload'][entity_type]['id']).strip().replace(crm_module_mapping[entity_type]['tab_id'],'')

    if(data['payload'].has_key('product')):
        try:
            prd_qb_id, data['payload']['product']['quickbooks_id']= data['payload']['product']['cf_847'], data['payload']['product']['cf_847']
            data['payload'][entity_type]['product_qbid'] = prd_qb_id
        except:
            pass

    if(data['payload'].has_key('account')):
        try:
            account_qbid, data['payload']['account']['quickbooks_id'] = data['payload']['account']['cf_809'], data['payload']['account']['cf_809']
            data['payload'][entity_type]['account_qbid'] = account_qbid
        except:
            pass

    if(data['payload'].has_key('potential')):
        try:
            potential_qbid, data['payload']['potential']['quickbooks_id'] = data['payload']['potential']['cf_894'], data['payload']['potential']['cf_894']
            data['payload'][entity_type]['potential_qbid'] = potential_qbid
        except:
            pass

    data_dct = {}
    if(data['payload'][entity_type]['LineItems']):

        for line_item in data['payload'][entity_type]['LineItems']:
            if (line_item['id'].find('x')<0):
                line_item['id'] = crm_module_mapping[entity_type]['tab_id'] + line_item['id']
                line_item['productid'] = crm_module_mapping['product']['tab_id'] + line_item['productid']

            sql_query = 'select lineitem_id from vtiger_inventoryproductrel where id=%s and productid = %s' %(line_item['id'].split('x')[1], line_item['productid'].split('x')[1])
            ret_data = get_db_query(sql_query, 'select')
            line_item['lineitem_id'] = str(ret_data[0][0])
            line_item['product_qbid'] = get_retrieve(line_item['productid'])['cf_847']

    try:
        current_balance = connect_to_quickbooks('account', 'query', data)['current_balance']
    except:
        current_balance = ''

    data_dct = connect_to_quickbooks(entity_type, action, data)
    data_dct['current_balance'] = current_balance

    resp_dct = {entity_type:data_dct}
    publish_to_queue(resp_dct, 'qb_update_'+entity_type)

def InvoicePDF(data, routing_key):

    publish_to_queue(data, 'InvoiceEmail')
    invoice_pdf = generate_invoice(data, routing_key, invoice_flag)

def QB_log(data, routing_key):

    js_data = json.dumps(data)

    if data.has_key('payload') and data['payload'].has_key('status'):
        if (data['payload']['status'].get('statusSeverity','')!='Info'):
            logger.error(js_data)
            email_generator("Error - QuickBooks Insertion Failure !!!", js_data)
        else:
            logger.info(js_data)
            #email_generator("QuickBooks Insertion Success !!!", js_data)
    else:
        logger.info(js_data)
